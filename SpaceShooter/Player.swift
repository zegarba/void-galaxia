//
//  Player.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 11/15/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation


class Player: SKSpriteNode, Updateable{
    
    var reload = 0.0
    var invincible = false
    var shieldCount = 9
    var dead = false
    
    private let explode = SKEmitterNode(fileNamed: "playerExplosion.sks")!
    var crosshair: Crosshair!
    
    private let sound = SKAction.playSoundFileNamed("shot02.m4a", waitForCompletion: false)
    private var gun1pos = CGPoint(x: 7, y: 14)
    private var gun2pos = CGPoint(x: -7, y: 14)
    private var bullet = PlayerBullet()
    private var bullet1: PlayerBullet!
    private var bullet2: PlayerBullet!
    
    static var playerPosition: CGPoint!
    
    var trail: SKEmitterNode{
        get {
            return childNode(withName: "trail") as! SKEmitterNode
        }
    }
    var barrier: SKSpriteNode{
        get {
            return childNode(withName: "barrier") as! SKSpriteNode
        }
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
    }
    
    convenience init(){
        self.init(image: #imageLiteral(resourceName: "PlayerShip1"))
        name = "player"
        zPosition = 2
        
        physicsBody = SKPhysicsBody(circleOfRadius: 2)
        physicsBody!.affectedByGravity = false
        physicsBody!.linearDamping = 0
        physicsBody!.collisionBitMask = 0
        physicsBody!.contactTestBitMask = 1
        
        bullet1 = bullet.copy() as! PlayerBullet
        bullet2 = bullet.copy() as! PlayerBullet
        
        let barrier = SKSpriteNode(imageNamed: "Barrier")
        barrier.name = "barrier"
        barrier.alpha = 0
        barrier.zPosition = 3
        addChild(barrier)
        
        let trail = SKEmitterNode(fileNamed: "playerTrail")!
        trail.position = CGPoint(x: 0, y: -18.5)
        trail.zPosition = 1
        trail.name = "trail"
        addChild(trail)
    }
    
    func addToScene(_ scene: GameScene){
        
        position = CGPoint(x: 0, y: scene.size.height * -0.35)
        scene.addChild(self)
        trail.targetNode = scene
        
        (childNode(withName: "/barrierCount") as? SKLabelNode)?.text = "Barriers: \(shieldCount)"
        crosshair = childNode(withName: "/crosshair") as! Crosshair
        
    }
    
    func shoot(){
        let target = crosshair.target
        if target != nil{
            
            
            let pos1 = CGPoint(x: self.position.x + gun1pos.x, y: self.position.y + gun2pos.y)
            let pos2 = CGPoint(x: self.position.x + gun2pos.x, y: self.position.y + gun2pos.y)
        
            scene!.addChild(bullet1)
            bullet1.shoot(at: target!, from: pos1)

            scene!.addChild(bullet2)
            bullet2.shoot(at: target!, from: pos2)
         
            run(sound)
            
   
            DispatchQueue.global(qos: .userInteractive).async {
                self.bullet1 = self.bullet.copy() as! PlayerBullet
                self.bullet2 = self.bullet.copy() as! PlayerBullet
            }
            
        }
    }
    

    func update(_ time: Double) {
        if !(scene! as! GameScene).over{
            if !dead{
                if reload >= 0.08 {
                    
                    self.shoot()
                    reload = 0
                    
                }
                else {
                    reload += time
                }
                
                if !(Remote.sharedInstance.controller?.microGamepad?.buttonX.isPressed)!{
                    move(GameScene.angle, time: time)
                }
            }
            
        }
        // copy position instead of pointing to it so that the player's position can't be modified
        // through the global variable
        Player.playerPosition = CGPoint(x: self.position.x, y: self.position.y)
    }

    
    func move(_ angle: Double, time: Double){
        if angle > 4 && position.x < (scene!.size.width / 2) - 50 {
            let speed = CGFloat((min(angle, 90)) * time * 6)
            position = CGPoint.init(x: position.x + speed, y: position.y)
        }
        else if angle < -4 && position.x > ((scene!.size.width / 2) * -1) + 50 {
            let speed = CGFloat((max(angle, -90)) * time * 6)
            position = CGPoint.init(x: position.x + speed, y: position.y)
        }
        
    }
    
    func takeDamage(){
        if invincible == false {
            if shieldCount > 0 {
                raiseShield()
            }
            else if !dead{
                die()
            }
        }
    }
    
    func raiseShield(){
        barrier.alpha = 1
        physicsBody?.contactTestBitMask = 0
        let fade = SKAction.fadeOut(withDuration: 3)
        fade.timingMode = .easeIn
        let end = SKAction.run {
            self.invincible = false
            self.physicsBody!.contactTestBitMask = 1
        }
        let sequence = SKAction.sequence([fade, end])
        barrier.run(sequence)
        invincible = true
        shieldCount -= 1
        (childNode(withName: "/barrierCount") as? SKLabelNode)?.text = "Barriers: \(shieldCount)"
    }
    
    func die(){
        dead = true
        physicsBody?.velocity = .init(dx: 0, dy: 0)
        
        childNode(withName: "/crosshair")?.run(.sequence([.fadeOut(withDuration: 0.5), .removeFromParent()]))
        trail.run(.fadeOut(withDuration: 0.5))
        
        
        physicsBody!.contactTestBitMask = 0
        let copy = explode.copy() as! SKEmitterNode
        copy.run(.sequence([.wait(forDuration: 2), .removeFromParent()]))
        copy.position = self.position
        scene!.addChild(copy)
        run(.playSoundFileNamed("longexplosion01.m4a", waitForCompletion: false))
        run(.sequence([.colorize(with: .red, colorBlendFactor: 1, duration: 0.25), .fadeOut(withDuration: 0.25)])){
            (self.scene as! GameScene).gameOver()
        }
    }
    
}
