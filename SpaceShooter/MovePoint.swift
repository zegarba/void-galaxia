//
//  MovePoint.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/3/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

struct MovePoint{
    
    enum MoveType {
        
        // Goes straight to point
        case linear
        
        // Moves Horizontally first
        case xCurve
        
        // Moves Vertically first
        case yCurve
    }
    
    enum SpeedType {
        case constant
        case slowDown
        case speedUp
        case speedUpSlowDown
    }
    
    let point: CGPoint
    let moveType: MoveType
    let speedType: SpeedType
    let delay: Double
    let speed: CGFloat?
    let time: Double?
    
    init(x: CGFloat, y: CGFloat, speed: CGFloat, moveType: MoveType, speedType: SpeedType, delay: Double){
        self.point = CGPoint(x: x, y: y)
        self.moveType = moveType
        self.speedType = speedType
        self.delay = delay
        self.speed = speed
        self.time = nil
    }

    init(x: CGFloat, y: CGFloat, time: Double, moveType: MoveType, speedType: SpeedType, delay: Double){
        self.point = .init(x: x, y: y)
        self.moveType = moveType
        self.speedType = speedType
        self.delay = delay
        self.time = time
        self.speed = nil
    }
    
    func getPath() -> SKAction{
        let path = CGMutablePath()
        path.move(to: CGPoint(x: 0, y: 0))
        
        switch moveType {
        case .linear:
            path.addLine(to: point)
        case .xCurve:
            path.addQuadCurve(to: point, control: CGPoint(x: point.x, y: 0))
        case .yCurve:
            path.addQuadCurve(to: point, control: CGPoint(x: 0, y: point.y))
        }
        

        var move = SKAction()
        
        if speed != nil {
            move = .follow(path, asOffset: true, orientToPath: false, speed: speed!)
        }
        else {
            move = .follow(path, asOffset: true, orientToPath: false, duration: time!)
        }
        switch speedType {
        case .slowDown:
            move.timingMode = .easeOut
        case .speedUp:
            move.timingMode = .easeIn
        case .speedUpSlowDown:
            move.timingMode = .easeInEaseOut
        default:
            move.timingMode = .linear
        }
        
        return SKAction.sequence([SKAction.wait(forDuration: delay), move])
    }
    
    
    static func path(points: [MovePoint]) -> SKAction{
        var actions = [SKAction]()
        
        for i in 0...points.count - 1{
            actions.append(points[i].getPath())
        }
        
        return SKAction.sequence(actions)
    }
}
