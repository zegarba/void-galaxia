//
//  DoubleExtension.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/24/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

extension Double {
    
    var toRadians: Double {
        return self * .pi / 180
    }
    var toDegrees: Double {
        return self * 180 / .pi
    }
    
    var cgfloat: CGFloat {
        return CGFloat(self)
    }
    
}

extension CGFloat {
    
    var double: Double {
        return Double(self)
    }
    
}

extension Int {
    var double: Double {
        return Double(self)
    }
    var cgfloat: CGFloat {
        return CGFloat(self)
    }
}


