//
//  PauseMenu.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 12/21/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import SpriteKit

class PauseMenu: SKScene{
    
    var select: SKLabelNode!
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
    }
    
    override func didMove(to view: SKView) {
    }
    
    func openMenu(){
        select = childNode(withName: "continue") as! SKLabelNode
        highlight(select)
    }
    
    func closeMenu(){
        unHilight(select)
        select = nil
    }
    
    func selectButton(){
        var actions = [SKAction]()
        
        select?.removeAllActions()
        select?.run(SKAction.scale(to: 1, duration: 0))
        
        
        if select?.name == "continue" {
            let sound = SKAction.playSoundFileNamed("confirm02.m4a", waitForCompletion: false)
            run(sound)
        }
        else {
            let sound = SKAction.playSoundFileNamed("quit01.m4a", waitForCompletion: false)
            run(sound)
        }
        
        for _ in 1...2{
            actions.append(SKAction.hide())
            actions.append(SKAction.wait(forDuration: 0.1))
            actions.append(SKAction.unhide())
            actions.append(SKAction.wait(forDuration: 0.1))
        }
        actions.append(SKAction.run {
            if self.select?.name == "continue" {
                ((self.view?.superview as! SKView).scene as! GameScene).pause()
            }
            else {
                MusicPlayer.stop()
                MusicPlayer.selectTrack(0)
                let scene = SKScene(fileNamed: "TitleScreen.sks")!
                scene.scaleMode = .aspectFit
                let mainView = self.view?.superview as! SKView
                self.view!.removeFromSuperview()
                let transition = SKTransition.fade(withDuration: 1)
                for gesture in mainView.gestureRecognizers!{
                    mainView.removeGestureRecognizer(gesture)
                }
                mainView.presentScene(scene, transition: transition)

            }
        })
        
        select?.run(SKAction.sequence(actions))

    }
    
    func moveSelect(){
        unHilight(select)
        if select.name == "continue" {
            select = childNode(withName: "quit") as! SKLabelNode!
            highlight(select)
        }
        else {
            select = childNode(withName: "continue") as! SKLabelNode!
            highlight(select)
        }
    }
    
    private func highlight(_ node: SKLabelNode){
        node.fontColor = UIColor.yellow
        node.run(SKAction.repeatForever(SKAction.sequence([SKAction.scale(to: 1.4, duration: 0.5), SKAction.scale(to: 1, duration: 0.5)])))
    }
    
    private func unHilight(_ node: SKLabelNode){
        node.fontColor = UIColor.white
        node.removeAllActions()
        node.run(SKAction.scale(to: 1, duration: 0))
    }
}
