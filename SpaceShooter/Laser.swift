//
//  Laser.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/4/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

class Laser: SKSpriteNode, Shootable, Updateable{
    
    var width: Double!
    var travelSpeed: Double!
    var angle: Double!
    
    private var stop = false
    
    var duration: Double!
    var fadeOutTime: Double!
    var currentTime: Double = 0
    
    
    convenience init(sprite: UIImage, width: Double, travelSpeed: Double, duration: Double, fade: Double, angle: Double){
        self.init(image: sprite)
        
        setCollisionDetection()
        
        self.width = width
        self.travelSpeed = travelSpeed / Double(size.height)
        self.duration = duration
        self.fadeOutTime = fade
        self.angle = angle
    }

    func copy() -> Shootable {
        let copy = super.copy() as! Laser
        
        copy.width = self.width
        copy.travelSpeed = self.travelSpeed
        copy.duration = self.duration
        copy.fadeOutTime = self.fadeOutTime
        
        copy.angle = self.angle
        return copy
    }
    
    func shoot(scene: SKScene) {
        
        addToGame(scene: scene)
        yScale = 0
        xScale = CGFloat(width)
        
    }
    
    func hitTarget(){      }
    
    func end() {
        var actions = [SKAction]()
        let fade = SKAction.fadeOut(withDuration: fadeOutTime)
        fade.timingMode = .easeIn
        actions.append(fade)
        actions.append(SKAction.removeFromParent())
        run(SKAction.sequence(actions))
    }
    
    func update(_ time: Double) {
        if currentTime > -1 {
            currentTime += time
        }
        if currentTime >= duration{
            currentTime = -1
            end()
        }
        
        if !stop {
            let height = size.height
            let x = sin(-zRotation) * height * 0.5
            let y = cos(-zRotation) * height * 0.5
            if position.y + y > -(scene!.size.height * 0.5) && position.x + x > -(scene!.size.width * 0.5) && position.x + x < scene!.size.width * 0.5 {
                yScale += CGFloat(travelSpeed * time)
                setCollisionDetection()
                let difference = size.height - height
                let xMove = sin(-zRotation) * difference * 0.5
                let yMove = cos(-zRotation) * difference * 0.5
                position = CGPoint(x: position.x + xMove, y: position.y + yMove)
            }
            else {
                stop = true
            }
        }
    }
}
