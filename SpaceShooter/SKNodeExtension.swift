//
//  SKSpriteNode Extension.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/23/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation

extension SKSpriteNode{
    
    convenience init(image: UIImage){
        self.init(texture: SKTexture(image: image))
    }
    
}
