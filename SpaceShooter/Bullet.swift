//
//  Bullet.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 11/16/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import SpriteKit
import GameplayKit

class Bullet: SKSpriteNode, Shootable, Updateable{
    
    
    
    var moveSpeed: Double!
    var moveRand: Double?
    
    // In degrees
    var angle: Double!
    var angleRand: Double?
    
    var acceleration: Double?
    var accRand: Double?
    
    var fallSpeed: Double?
    var fallRand: Double?
    
    var duration: Double?
    
    private var nextStep: ShotSequence?
    var volleyNo = 0
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        moveSpeed = aDecoder.decodeObject(forKey: "move") as! Double
        acceleration = aDecoder.decodeObject(forKey: "acc") as? Double
        angle = aDecoder.decodeObject(forKey: "angle") as! Double
        
        duration = aDecoder.decodeObject(forKey: "dur") as? Double
        nextStep = aDecoder.decodeObject(forKey: "next") as? ShotSequence
        
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        
        aCoder.encode(moveSpeed, forKey: "move")
        aCoder.encode(acceleration, forKey: "acc")
        aCoder.encode(angle, forKey: "angle")
        
        aCoder.encode(duration, forKey: "dur")
        aCoder.encode(nextStep, forKey: "next")
    }
        
    convenience init(sprite: UIImage, speed: Double, angle: Double) {
        self.init(image: sprite)
        moveSpeed = speed
        self.angle = angle
    }
    
    func copy() -> Shootable {
        let copy = super.copy() as! Bullet
            
        copy.moveSpeed = self.moveSpeed
        copy.moveRand = self.moveRand
        copy.angle = self.angle
        copy.angleRand = self.angleRand
        copy.acceleration = self.acceleration
        copy.accRand = self.accRand
        copy.fallSpeed = self.fallSpeed
        copy.fallRand = self.fallRand
        copy.duration = self.duration
        if self.nextStep != nil {
            copy.nextStep = ShotSequence(self.nextStep!)
        }
        
        return copy
    }
    
    func setNextStep(_ sequence: ShotSequence){
        self.nextStep = ShotSequence(sequence)
    }
    
    func shoot(scene: SKScene){
        
        // add randomization to bullet
        moveSpeed = randomize(moveSpeed, with: moveRand)
        self.angle = randomize(self.angle, with: angleRand)
        acceleration = randomize(acceleration, with: accRand)
        fallSpeed = randomize(fallSpeed, with: fallRand)
        
        setCollisionDetection()
        addToGame(scene: scene)
        
        let angle = self.angle.toRadians
        
        if nextStep != nil {
            DispatchQueue.global(qos: .userInitiated).async {
                var sequence = [SKAction]()
                
                for grp in self.nextStep!.volleys{
                    var group = [SKAction]()
                    for volley in grp{
                        var rounds = [SKAction]()
                        for i in 0...volley.rounds-1{
                            rounds.append(.wait(forDuration: volley.delay))
                            rounds.append(.run {
                                volley.shootVolley(from: self, at: Player.playerPosition, round: i, with: self.angle)
                                })
                            /**if volley.shootSound != nil {
                                rounds.append(volley.shootSound!)
                            }**/
                            rounds.append(.wait(forDuration: volley.timeBetweenRounds))
                        }
                        rounds.removeLast()
                        rounds.append(.wait(forDuration: volley.postDelay))
                        group.append(.sequence(rounds))
                    }
                    sequence.append(.group(group))
                }
                
                sequence.append(.removeFromParent())
                let shoots = SKAction.sequence(sequence)
                shoots.speed = 0
                self.run(shoots, withKey: "shoots")
                self.action(forKey: "shoots")?.speed = 0
            }
        }
        
        //Set speed
        let xSpeed = moveSpeed * sin(angle)
        let ySpeed = moveSpeed * cos(angle)
        physicsBody?.velocity = CGVector(dx: xSpeed, dy: ySpeed)
        
        
        // create ending action if bullet has a duration
        if duration != nil {
            let endAction = SKAction.sequence([SKAction.wait(forDuration: duration!), SKAction.perform(#selector(end), onTarget: self)])
            run(endAction)
            
        }
    }
    

    
    func hitTarget() {
        removeFromParent()
    }
    
    @objc func end(){
        if nextStep != nil {
            action(forKey: "shoots")?.speed = 1
        }
        
        else {
            run(.removeFromParent())
        }
    }

    func update(_ time: Double){
        if acceleration != nil{
            moveSpeed! += acceleration! * time
            if moveSpeed > 0 {
                let xMove = CGFloat(sin(angle.toRadians) * moveSpeed)
                let yMove = CGFloat(cos(angle.toRadians) * moveSpeed)
                physicsBody!.velocity = CGVector(dx: xMove, dy: yMove)
            }
            else {
                physicsBody!.velocity = .init(dx: 0, dy: 0)
            }
        }
        
        if fallSpeed != nil{
            physicsBody!.velocity = CGVector(dx: physicsBody!.velocity.dx, dy: physicsBody!.velocity.dy - CGFloat(fallSpeed!) * CGFloat(time))
        }
        
        if !intersects(parent!){
            removeFromParent()
        }
    }
    
}

