//
//  StageCrafter.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 12/8/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

extension GameScene{
    
    static func getStage(_ stageNumber: Int) -> GameScene {
        let scene = SKScene(fileNamed: "GameScene") as! GameScene
        scene.scaleMode = .aspectFill
        scene.stageNo = stageNumber
        return scene
    }
    
    func setStage(){
        
        let player = Player()
        let startPlayer = SKSpriteNode(image: #imageLiteral(resourceName: "PlayerShip1"))
        startPlayer.position = CGPoint(x: 0, y: size.height * -0.6)
        let moveAction = SKAction.move(to: CGPoint.init(x: 0, y: size.height * -0.35), duration: 3)
        moveAction.timingMode = .easeOut
        
        startPlayer.run(moveAction){
            startPlayer.removeFromParent()
            player.addToScene(self)
        }
        
        let trail = SKEmitterNode(fileNamed: "playerTrail")!
        trail.position = CGPoint(x: 0, y: -18.5)
        trail.zPosition = 1
        trail.name = "trail"
        trail.targetNode = self
        startPlayer.addChild(trail)
        addChild(startPlayer)
        
        let crosshair = Crosshair()
        crosshair.addToScene(self, at: CGPoint(x:0, y:0))
        
        setLevel()
        
        
    }
    
    private func setLevel(){

        switch stageNo {
        case 0:
            stage0()
        case 2:
            stage2()
        case 3:
            stage3()
        default:
            stage1()
        }
        
    }
    
    func craftPauseMenu(){
        let pause = SKView(frame: CGRect(x: 0, y: 0, width: 512, height: 256))
        let scene = SKScene(fileNamed: "PauseMenu")
        view?.addSubview(pause)
        pause.tag = 3
        pause.presentScene(scene)
        pause.isHidden = true
        pause.isPaused = true
        pause.center = pause.superview!.center
    }
    
    func getMoveTime(point1: CGPoint, point2: CGPoint, speed: CGFloat) -> Double{
        let distance = hypot(point2.x - point1.x, point2.y - point1.y)
        return Double(distance / speed)
        
    }

}
