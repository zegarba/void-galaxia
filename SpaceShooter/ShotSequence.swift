//
//  ShotSequence.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 2/20/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

class ShotSequence{
    
    var volleys = [[Volley]]()
    
    
    private init(sequenceOfGroups volleys: [[Volley]]){
        self.volleys = volleys
    }
    
    private init(sequence volleys: [Volley]){
        for volley in volleys {
            self.volleys.append([volley])
        }
    }
    
    private init(group volleys: [Volley]){
        self.volleys = [volleys]
    }
    
    private init(single volley: Volley){
        self.volleys = [[volley]]
    }
    
    class func sequenceOfGroups(_ volleys: [[Volley]]) -> ShotSequence {
        return ShotSequence(sequenceOfGroups: volleys)
    }
    
    class func sequence(_ volleys: [Volley]) -> ShotSequence {
        return ShotSequence(sequence: volleys)
    }
    
    class func group(_ volleys: [Volley]) -> ShotSequence {
        return ShotSequence(group: volleys)
    }
    
    class func single(_ volley: Volley) -> ShotSequence {
        return ShotSequence(single: volley)
    }
    
    init(_ shotSequence: ShotSequence){
        for group in shotSequence.volleys{
            var volleyGroup = [Volley]()
            for volley in group{
                volleyGroup.append(Volley(volley))
            }
            volleys.append(volleyGroup)
        }
    }
    
}
