//
//  Target.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 11/29/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

class Crosshair: SKSpriteNode, Updateable{
    
    var target: Enemy?
    var moving = false
    var topBoundary:CGFloat = 0.0
    var bottomBoundary:CGFloat = 0.0
    var leftBoundary:CGFloat = 0.0
    var rightBoundary:CGFloat = 0.0

    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)

    }
    
    convenience init(){
        self.init(image: #imageLiteral(resourceName: "Target"))
        name = "crosshair"
        zPosition = 10
        
        run(.scale(to: 0.5, duration: 0))
        
        physicsBody = SKPhysicsBody(circleOfRadius: 150)
        physicsBody!.affectedByGravity = false
        physicsBody!.collisionBitMask = 0
        physicsBody!.categoryBitMask = 2
        physicsBody!.linearDamping = 0
        run(SKAction.repeatForever(SKAction.rotate(byAngle: 0.5, duration: 1)))
    }
    
    func addToScene(_ scene: GameScene, at point: CGPoint){
        self.position = point
        topBoundary = CGFloat(0.49) * scene.size.height
        bottomBoundary = CGFloat(-0.49) * scene.size.height
        leftBoundary = CGFloat(-0.49) * scene.size.width
        rightBoundary = CGFloat(0.49) * scene.size.width
        scene.addChild(self)
    }
    
    func isInBounds(_ point: CGPoint) -> Bool {
        if point.y < topBoundary && point.x < rightBoundary && point.y > bottomBoundary && point.x > leftBoundary {
            return true
        }
        else {
            return false
        }
    }
    
    func move(by point: CGPoint){
        var x = position.x + point.x
        var y = position.y + point.y
        x = min(max(x, leftBoundary), rightBoundary)
        y = min(max(y, bottomBoundary), topBoundary)
        position = CGPoint(x: x, y: y)
    }
    
    func unlock(){
        target = nil
        moving = true
        run(SKAction.scale(to: 1, duration: 0.1))
    }
    
    func lock(){
        moving = false
        run(SKAction.scale(to: 0.5, duration: 0.1))
    }
    
    func lockOn(){
        let possibleTargets = physicsBody!.allContactedBodies()
        var targets = [Enemy]()
        for t in possibleTargets{
            if t.node != nil{
                if let node = t.node as? Enemy{
                    if isInBounds(node.position) && node.isDead == false {
                        targets.append(node)
                    }
                }
                else if let node = t.node as? EnemyPart{
                    let parent = node.parent as! Enemy
                    if isInBounds(parent.position) && parent.isDead == false {
                        targets.append(parent)
                    }
                }
            }
        }
        
        if targets.isEmpty == false {
            target = chooseTarget(from: targets)
        }
    }
    
    func update(_ time: Double) {
        if target != nil {
            if target?.isDead == false {
                if isInBounds(target!.position){
                let difference = CGPoint(x: target!.position.x - position.x, y: target!.position.y - position.y)
                    position = CGPoint(x: position.x + difference.x * 0.5, y: position.y + difference.y * 0.1)
                }
                else {
                    target = nil
                }
            }
            else {
                target = nil
            }
        }
        else{
            if moving == false{
                lockOn()
            }
        }
    }
    
    func chooseTarget(from enemies: [Enemy]) -> Enemy{
        var target: Enemy!
        var priority = -1
        
        for enemy in enemies {
            if priority == -1 {
                target = enemy
                priority = enemy.priority
            } else if priority > enemy.priority {
                target = enemy
                priority = enemy.priority
            } else if priority == enemy.priority {
                if getDistance(to: enemy.position) < getDistance(to: target.position){
                    target = enemy
                }
            }
    
        }
        return target
    }
    
    private func getDistance(to point: CGPoint) -> CGFloat{
        return hypot(point.x - position.x, point.y - position.y)
    }
}
