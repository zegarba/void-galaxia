//
//  Gun.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 12/15/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

class Gun: SKNode{
    

    

    var delay: Double!
    var delayRand: Double?
    var reload: Double!
    
    var volleys: ShotSequence!
    
    override init(){
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
    }
    
    override func copy() -> Any {
        let copy = super.copy() as! Gun
        copy.delay = self.delay
        copy.delayRand = self.delayRand
        copy.reload = self.reload
        copy.volleys = ShotSequence.init(self.volleys)
        return copy
    }
    
    convenience init(delay: Double, reload: Double, volleys: ShotSequence, position: CGPoint){
        self.init()
        self.position = position
        
        self.delay = delay
        self.reload = reload
        self.volleys = ShotSequence(volleys)
        
    }
    
    func startFiringSequence(){
        
        if delayRand != nil {
            delay! += drand48() * delayRand!
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            var sequence = [SKAction]()
            for grp in self.volleys.volleys{
                var group = [SKAction]()
                for volley in grp{
                    var rounds = [SKAction]()
                    rounds.append(.wait(forDuration: volley.delay))
                    for i in 0...volley.rounds - 1 {
                        rounds.append(.run {
                            volley.shootVolley(from: self, at: Player.playerPosition, round: i, with: 180)
                            })
                        /**if volley.shootSound != nil {
                            rounds.append(volley.shootSound!)
                        }**/
                        rounds.append(.wait(forDuration: volley.timeBetweenRounds))
                    }
                    rounds.removeLast()
                    rounds.append(.wait(forDuration: volley.postDelay))
                    group.append(.sequence(rounds))
                }
                sequence.append(.group(group))
            }
            
            sequence.append(.wait(forDuration: self.reload))
            
            let shoots = SKAction.repeatForever(.sequence(sequence))
            self.run(shoots, withKey: "shoots")
            self.action(forKey: "shoots")?.speed = 0

        }
        
        run(.wait(forDuration: delay)){
            self.action(forKey: "shoots")?.speed = 1
        }
        
    }
    
}
