//
//  InstantLaser.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/5/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

class InstantLaser: SKSpriteNode, Shootable, Updateable{
    
    var angle: Double!
    var width: Double!
    
    var delay: Double!
    var duration: Double!
    var currentTime: Double = 0
    
    private var stop = false
    
    private var s: SKAction?
    var sound: String? {
        didSet {
            if let s = sound {
                self.s = .playSoundFileNamed(s, waitForCompletion: false)
            } else {
                self.s = nil
            }
        }
    }
    
    convenience init(sprite: UIImage, angle: Double, delay: Double, duration: Double, width: Double){
        self.init(image: sprite)
        setCollisionDetection()
        self.angle = angle
        self.delay = delay
        self.duration = duration
        self.width = width
        
    }
    
    convenience init(_ laser: InstantLaser){
        self.init(texture: laser.texture, color: laser.color, size: laser.size)
        
        self.colorBlendFactor = laser.colorBlendFactor
        
        self.angle = laser.angle
        self.delay = laser.delay
        self.duration = laser.duration
        self.width = laser.width
        self.sound = laser.sound
        self.s = laser.s
    }
    
    func copy() -> Shootable{
        let copy = super.copy() as! InstantLaser
        
        copy.angle = self.angle
        copy.delay = self.delay
        copy.duration = self.duration
        copy.width = self.width
        copy.sound = self.sound
        copy.s = self.s
    
        return copy
    }
    
    func shoot(scene: SKScene){
    
        xScale = CGFloat(width)
        
        addToGame(scene: scene)
        
        yScale = 0
        
        let tracer = SKSpriteNode(texture: texture)
        tracer.xScale = CGFloat(width) * 0.15
        
        tracer.zRotation = zRotation
        tracer.position = position
        tracer.run(SKAction.scaleX(to: 0.15 * width.cgfloat, y: 60, duration: 0.2))
        let tHeight = tracer.size.height
        let xMove = sin(-zRotation) * tHeight * 30
        let yMove = cos(-zRotation) * tHeight * 30
        tracer.run(SKAction.moveBy(x: xMove, y: yMove, duration: 0.2))
        tracer.alpha = 0.3
        tracer.color = self.color
        tracer.colorBlendFactor = self.colorBlendFactor
        
        
        tracer.run(SKAction.sequence([SKAction.wait(forDuration: Double(delay)), SKAction.fadeOut(withDuration: 0.3), SKAction.removeFromParent()]))
        
        scene.addChild(tracer)
        
    }
    
    func hitTarget(){    }
    
    func end(){
        
        physicsBody?.categoryBitMask = 0
        
        var actions = [SKAction]()
        actions.append(.fadeOut(withDuration: 0.3))
        actions.append(SKAction.removeFromParent())
        run(SKAction.sequence(actions))
    }
    
    func update(_ time: Double){
        if currentTime > -1{
            currentTime += Double(time)
        }
        
        if currentTime >= delay{
            
            if !stop {
                let height = size.height
                let x = sin(-zRotation) * height * 0.5
                let y = cos(-zRotation) * height * 0.5
                if position.y + y > -(scene!.size.height * 0.5) && position.x + x > -(scene!.size.width * 0.5) && position.x + x < scene!.size.width * 0.5 {
                    yScale += CGFloat(100 * time)
                    setCollisionDetection()
                    let difference = size.height - height
                    let xMove = sin(-zRotation) * difference * 0.5
                    let yMove = cos(-zRotation) * difference * 0.5
                    position = CGPoint(x: position.x + xMove, y: position.y + yMove)
                }
                else {
                    stop = true
                }
            }
        }
        if currentTime >= (delay + duration) {
            end()
            currentTime = -1
        }
    }
}
