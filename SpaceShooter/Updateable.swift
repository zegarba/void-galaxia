//
//  Updateable.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 8/8/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

protocol Updateable {
    func update(_ time: Double)
}
