//
//  Volley.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/25/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

class Volley {
    
    enum AimType {
        // no special aim
        case none
        // uses aim of previous bullet if applicable
        case previous
        // aims rounds toward player
        case player
        // aims first round toward player, aims the rest in the same direciont
        case playerFreeze
    }
    

    
    let bullets: [Shootable]
    let rounds: Int
    let timeBetweenRounds: Double
    
    // private var currentRound = 0
    
    private let aim: AimType
    
    // 180 sets default shot angle to straight down instead of straight up, will change for aim types
    private var aimAngle: Double = 180
    
    var delay: Double = 0
    var postDelay: Double = 0
    
    var angle: Double = 0
    var angleChange: Double = 0
    var angleRand: Double = 0
    private var rand: Double = 0
    
    var x: CGFloat = 0
    var y: CGFloat = 0
    var xChange: CGFloat = 0
    var yChange: CGFloat = 0
    
    var shootSound: SKAction?
    var sound: String? = nil {
        didSet {
            if let s = sound {
                shootSound = SKAction.playSoundFileNamed(s, waitForCompletion: false)
            } else {
                shootSound = nil
            }
        }
    }
    
    
    init(bullets: [Shootable], rounds: Int, timeBetweenRounds: Double, aim: AimType){
        self.bullets = bullets
        self.rounds = rounds
        self.timeBetweenRounds = timeBetweenRounds
        self.aim = aim
    }
    
    init(_ volley: Volley){
        self.bullets = volley.bullets
        self.rounds = volley.rounds
        self.timeBetweenRounds = volley.timeBetweenRounds
        self.aim = volley.aim
        self.delay = volley.delay
        self.postDelay = volley.postDelay
        self.angle = volley.angle
        self.angleChange = volley.angleChange
        self.angleRand = volley.angleRand
        self.x = volley.x
        self.y = volley.y
        self.xChange = volley.xChange
        self.yChange = volley.yChange
        self.shootSound = volley.shootSound
        self.sound = volley.sound
    }
    
    // legacy initializer
    init(bullets: [Shootable], rounds: Int, timeBetweenRounds: Double, aim: AimType, angle: Double, angleChange: Double?, sound: String?){
        self.bullets = bullets
        self.rounds = rounds
        self.timeBetweenRounds = timeBetweenRounds
        self.aim = aim
        self.angle = angle
        if angleChange == nil{
            self.angleChange = 0
        }
        else {
            self.angleChange = angleChange!
        }
        self.sound = sound
        if sound != nil {
            self.shootSound = SKAction.playSoundFileNamed(sound!, waitForCompletion: false)
        }
    }

    func shootVolley(from shooter: SKNode, at target: CGPoint, round: Int, with previousAngle: Double) {
        
        
        if shootSound != nil {
            shooter.run(shootSound!)
        }
        
        let position = shooter.convert(.init(), to: shooter.scene!)
        
        switch self.aim {
        case .none:
            aimAngle = 180
        case .previous:
            aimAngle = previousAngle
        case .player:
            aimAngle = atan2(target.x - position.x, target.y - position.y).double.toDegrees
        case .playerFreeze:
            if round == 0 {
                aimAngle = atan2(target.x - position.x, target.y - position.y).double.toDegrees
            }
        }
        
        if self.angleRand != 0 {
            self.rand = self.angleRand * drand48() * 2 - self.angleRand
        }
        
        for bullet in self.bullets {
            let copy = bullet.copy()
            let posX = position.x + x + xChange * round.cgfloat
            let posY = position.y + y + yChange * round.cgfloat
            copy.position = CGPoint(x: posX, y: posY)
            copy.angle! += self.angle + self.angleChange * round.double + self.aimAngle + self.rand
            
            
            copy.shoot(scene: shooter.scene!)
        }
        
    }
}
