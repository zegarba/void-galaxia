//
//  AudioPlayer.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 8/8/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation

class MusicPlayer {
    private static var music = AVAudioPlayer()
    private static var trackSelection = 0
    
    static func stop(){
        music.stop()
    }
    
    static func pause(){
        music.pause()
    }
    
    static func resume(){
        music.play()
    }
    
    static func selectTrack(_ track: Int){
        trackSelection = track
    }
    
    static func fanfare(){
        
        let url = Bundle.main.url(forResource: "Fanfare", withExtension: "m4a")!
        music = try! AVAudioPlayer(contentsOf: url)
        music.play()
    }
    
    static func fadeOut(){
        music.setVolume(0, fadeDuration: 1)
    }
    
    static func playBoss(filename: String){
        playBoss(filename: filename, loop: true)
    }

    static func playBoss(filename: String, loop: Bool){
        let url = Bundle.main.url(forResource: filename, withExtension: nil)!
        music = try! AVAudioPlayer(contentsOf: url)
        if loop {
            music.numberOfLoops = -1
        }
        music.play()
    }
    
    static func play(){
        switch trackSelection {
        case 0:
            let url = Bundle.main.url(forResource: "Title Music 2", withExtension: "m4a")!
            music = try! AVAudioPlayer(contentsOf: url)
        case 2:
            let url = Bundle.main.url(forResource: "Stage02", withExtension: "m4a")!
            music = try! AVAudioPlayer(contentsOf: url)
        case 3:
            let url = Bundle.main.url(forResource: "Stage03", withExtension: "m4a")!
            music = try! AVAudioPlayer(contentsOf: url)
        default:
            let url = Bundle.main.url(forResource: "Stage01", withExtension: "m4a")!
            music = try! AVAudioPlayer(contentsOf: url)
        }
        music.numberOfLoops = -1
        music.play()
    }
}
