//
//  Stage0.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/20/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//
//  Used for testing purposes only!


import Foundation
import SpriteKit

extension GameScene{
    
    func stage0(){
        
        stageNo = 0
        
        var wave = [Enemy]()
        let enemy = Enemy.init(priority: 1, life: 999999999, delay: 1, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
        enemy.position = .init(x: 0, y: 200)
        wave.append(enemy)
        waves.append(wave)
    }
}
