//
//  EnemyPart.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/30/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

class EnemyPart: SKSpriteNode{
    
    var deathAnim: SKAction?
    
    private var explode: SKEmitterNode?
    var explosion: String? {
        didSet {
            if let ex = explosion {
                explode = SKEmitterNode(fileNamed: ex)
            }
            else {
                explode = nil
            }
        }
    }
    
    
    convenience init(sprite: UIImage) {
        self.init(image: sprite)
        self.physicsBody = SKPhysicsBody(texture: self.texture!, size: self.size)
        physicsBody!.affectedByGravity = false
        physicsBody!.linearDamping = 0
        physicsBody!.collisionBitMask = 0
        physicsBody!.contactTestBitMask = 2
    }
    
    func die(){
        physicsBody!.contactTestBitMask = 0
        if deathAnim != nil {
            run(.sequence([deathAnim!, .removeFromParent()]))
            move(toParent: scene!)
        }
        if explosion != nil {
            let copy = explode!.copy() as! SKEmitterNode
            addChild(copy)
            copy.targetNode = scene!
        }
    }
    
}
