//
//  Enemy.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 11/29/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

class Enemy: SKSpriteNode {
    
    var priority: Int!
    var life: Int!
    var delay: Double!
    
    // action that is run when added to game
    var phaseIn: SKAction?
    
    // actions that is run before moving out
    // can be used to reposition enemy before next phase is triggered
    // start with default fade out
    var phaseOut: SKAction = .fadeOut(withDuration: 0.25)
    
    var explode: SKEmitterNode?
    private var sound: SKAction?

    // Explosion Effects
    var explosion: String? {
        didSet {
            if let ex = explosion {
                explode = SKEmitterNode(fileNamed: ex)
            }
            else {
                explode = nil
            }
        }
    }
    
    var explodeSound: String? {
        didSet {
            if let s = explodeSound {
                sound = SKAction.playSoundFileNamed(s, waitForCompletion: false)
            }
            else {
                sound = nil
            }
        }
    }
    
    // for multi-phase enemies, like bosses
    // will add to current position when removed from game
    var nextPhase: Enemy?
    
    
    // private values
    private var dead = false
    var isDead: Bool {
        get {
            return dead
        }
    }
    private var set = false
    var isSet: Bool {
        get {
            return set
        }
    }
    
    var isBoss = false
    var bossMusic: String?
    var bossLoop = true
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        priority = aDecoder.decodeInteger(forKey: "priority")
        life = aDecoder.decodeInteger(forKey: "life")
        delay = aDecoder.decodeDouble(forKey: "delay")
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(priority!, forKey: "priority")
        aCoder.encode(life!, forKey: "life")
        aCoder.encode(delay!, forKey: "delay")
    }
    
    override func copy() -> Any {
        let copy = super.copy() as! Enemy
        copy.priority = priority
        copy.life = life
        copy.delay = delay
        copy.phaseIn = phaseIn
        copy.phaseOut = phaseOut
        copy.explosion = explosion
        copy.explodeSound = explodeSound
        copy.enumerateChildNodes(withName: "gun"){gun, _ in
            gun.removeFromParent()
        }
        enumerateChildNodes(withName: "gun"){ gun, _ in
            copy.addGun(gun as! Gun)
        }
        
        return copy
    }
    
    convenience init(priority: Int, life: Int, delay: Double, sprite: UIImage){
        self.init(image: sprite)
        name = "enemy"
        
        physicsBody = SKPhysicsBody(texture: texture!, size: size)
        physicsBody!.affectedByGravity = false
        physicsBody!.linearDamping = 0
        physicsBody!.collisionBitMask = 0
        physicsBody!.contactTestBitMask = 2
        
        self.priority = priority
        self.life = life
        self.delay = delay
        self.zPosition = 3
    }
    
    func setMovement(start: [MovePoint]?, loop: [MovePoint]?, remove: Bool){
        var actions = [SKAction]()
        if start != nil {
            actions.append(MovePoint.path(points: start!))
        }
        
        if loop != nil {
            actions.append(SKAction.repeatForever(MovePoint.path(points: loop!)))
        }
        else if remove {
            actions.append(.removeFromParent())
        }
        
        run(SKAction.sequence(actions), withKey: "move")
    }
    
    func addPart(_ part: EnemyPart) {
        addChild(part)
        part.name = "enemy part"
    }
    
    func addGun(_ gun: Gun){
        let copy = gun.copy() as! Gun
        addChild(copy)
        copy.name = "gun"
    }
    
    func addGuns(_ guns: [Gun]){
        for gun in guns{
            addGun(gun)
        }
    }
    
    func spin(_ speed: Double){
        
        // if this is called without any turn speed return without doing anything
        if speed == 0 {
            return
        }
        
        var angle = CGFloat.pi * 2
        var turnSpeed = speed
        if speed < 0 {
            angle = -angle
            turnSpeed = -turnSpeed
        }
        
        run(.repeatForever(.rotate(byAngle: angle, duration: 1.0 / turnSpeed)))
    }
    
    func addToScene(_ scene: SKScene){
        
        if bossMusic != nil {
            MusicPlayer.fadeOut()
        }
        
        scene.run(SKAction.wait(forDuration: delay)){
            
            if self.phaseIn != nil{
                
                let fakeNode = SKSpriteNode.init(texture: self.texture!)
                for child in self.children{
                    if child.name != "gun"{
                        let copy = SKSpriteNode(texture: (child as! SKSpriteNode).texture!)
                        copy.position = child.position
                        fakeNode.addChild(copy)
                    }
                }
                fakeNode.alpha = self.alpha
                fakeNode.position = self.position
                fakeNode.zPosition = self.zPosition
                scene.addChild(fakeNode)
                
                fakeNode.run(self.phaseIn!){
                    self.position = fakeNode.position
                    self.alpha = fakeNode.alpha
                    fakeNode.removeFromParent()
                    scene.addChild(self)
                    self.set = true
                    self.enumerateChildNodes(withName: "gun"){ gun, _ in
                        (gun as! Gun).startFiringSequence()
                    }
                    if let music = self.bossMusic {
                        MusicPlayer.playBoss(filename: music, loop: self.bossLoop)
                    }
                    
                }
                
            }
                
            else {
                self.set = true
                scene.addChild(self)
                self.enumerateChildNodes(withName: "gun"){ gun, _ in
                    (gun as! Gun).startFiringSequence()
                }
                if let music = self.bossMusic {
                    MusicPlayer.playBoss(filename: music, loop: self.bossLoop)
                }
            }
        }
    }
    
    func takeDamage(_ damage: Int){
        life = life! - damage
        if isBoss {
            lifeBarLower(damage)
        }
        if life < 1 && isDead == false{
            die()
        }
    }
    
    private func lifeBarLower(_ damage: Int){
        let lifeBar = childNode(withName: "/lifebar")!
        let totalLife = lifeBar.userData!["life"] as! Int
        lifeBar.yScale -= 56.cgfloat / totalLife.cgfloat
    }
    
    func die(){
        dead = true
        physicsBody!.contactTestBitMask = 0
        
        if explosion != nil {
            if explode != nil {
                let copy = explode!.copy() as! SKEmitterNode
                copy.position = .init(x: self.position.x + explode!.position.x, y: self.position.y + explode!.position.y)
                copy.zPosition = self.zPosition + 1
                let time = (CGFloat(explode!.numParticlesToEmit) / explode!.particleBirthRate) + explode!.particleLifetime
                copy.run(SKAction.sequence([SKAction.wait(forDuration: Double(time)), SKAction.removeFromParent()]))
                scene!.addChild(copy)
            }
        }
        if explodeSound != nil {
            run(sound!)
        }
        removeAction(forKey: "move")
        enumerateChildNodes(withName: "gun") {
            gun, _ in
            gun.removeFromParent()
        }
        
        var deathSequence = [SKAction]()
        deathSequence.append(phaseOut)
        
        
        if isBoss {
            deathSequence.append(.perform(#selector(removeLifeBar), onTarget: self))
            if nextPhase == nil {
                // end of stage, make sure player doesn't get hit afterward
                childNode(withName: "/player")?.physicsBody?.contactTestBitMask = 0
                MusicPlayer.fadeOut()
            }
        }
        
        enumerateChildNodes(withName: "enemy part") { part, _ in
            (part as! EnemyPart).die()
        }
        if nextPhase != nil {
            deathSequence.append(SKAction.run {
                self.nextPhase!.position = self.position
                (self.scene! as! GameScene).enemies.append(self.nextPhase!)
                self.nextPhase!.addToScene(self.scene!)
            })
        }
        deathSequence.append(SKAction.removeFromParent())
        run(SKAction.sequence(deathSequence))

    }
    
    @objc private func removeLifeBar(){
        let lifebar = childNode(withName: "/lifebar") as! SKSpriteNode
        let text = childNode(withName: "/bosslife") as! SKLabelNode
        if nextPhase == nil {
            text.run(.fadeOut(withDuration: 0.5))
            lifebar.run(.fadeOut(withDuration: 0.5))
            
        }
        else {
            lifebar.userData!["life"] = nextPhase!.life
            lifebar.run(.scaleY(to: 56, duration: 0.2))
            
        }
    }
    
}

