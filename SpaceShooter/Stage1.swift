//
//  stage1.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/3/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation

extension GameScene{
    
    func stage1(){
        
        // set background stars
        let stars = SKEmitterNode(fileNamed: "stage1Background")!
        stars.position = CGPoint(x: 0, y: size.height * 0.5)
        stars.zPosition = -1
        stars.name = "stars"
        addChild(stars)
        stars.advanceSimulationTime(5)

        /**let musicURL = Bundle.main.url(forResource: "Stage01", withExtension: "m4a")
        if musicURL == nil {
            print("no music!")
        }
        musicPlayer = try! AVAudioPlayer(contentsOf: musicURL!)
        musicPlayer!.numberOfLoops = -1
        musicPlayer!.play()**/
        
        
        
        // set enemies
        var wave1 = [Enemy]()
        
        let explode = "explosion"
        
        let baseSpeed: CGFloat = 300

        var bullets1 = [Shootable]()
        for i in 0...2 {
            let d = Double(i)
            let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 350, angle: -20.0 + d * 20.0)
            bullet.color = .cyan
            bullet.colorBlendFactor = 1
            bullets1.append(bullet)
        }
        
        let volley1 = Volley(bullets: bullets1, rounds: 2, timeBetweenRounds: 1.5, aim: .player, angle: 0, angleChange: 0, sound: "shot01.m4a")
        let spreadGun1 = Gun(delay: 1.5, reload: 20, volleys: .single(volley1), position: .init(x: 0, y: 0))
        
        let motion1 = [MovePoint.init(x: 0, y: -200, speed: baseSpeed, moveType: .linear, speedType: .constant, delay: 0), MovePoint.init(x: 200, y: -200, speed: baseSpeed, moveType: .yCurve, speedType: .constant, delay: 0), MovePoint.init(x: 800, y: 0, speed: baseSpeed, moveType: .linear, speedType: .constant, delay: 0)]
        
        for i in 1...6 {
            let d = Double(i)
            let enemy = Enemy.init(priority: i, life: 9, delay: d * 0.2, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
            enemy.explosion = explode
            enemy.explodeSound = "explosion01.m4a"
            enemy.position = CGPoint(x: -400, y: 450)
            enemy.setMovement(start: motion1, loop: nil, remove: true)
            enemy.addGun(spreadGun1)
            enemy.spin(-1)
            wave1.append(enemy)
        }
        
        waves.append(wave1)
        
        var wave2 = [Enemy]()
        
        let motion2 = [MovePoint.init(x: 0, y: -200, speed: baseSpeed, moveType: .linear, speedType: .constant, delay: 0), MovePoint.init(x: -200, y: -200, speed: baseSpeed, moveType: .yCurve, speedType: .constant, delay: 0), MovePoint.init(x: -800, y: 0, speed: baseSpeed, moveType: .linear, speedType: .constant, delay: 0)]
        
        for i in 1...6 {
            let d = Double(i)
            
            let enemy = Enemy(priority: i, life: 9, delay: d * 0.2, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
            enemy.explosion = explode
            enemy.explodeSound = "explosion01.m4a"
            enemy.position = CGPoint(x: 400, y: 450)
            enemy.setMovement(start: motion2, loop: nil, remove: true)
            enemy.addGun(spreadGun1)
            enemy.spin(1)
            wave2.append(enemy)
        }
        
        waves.append(wave2)
        
        var wave3 = [Enemy]()
        
        let laserVolley1 = Volley(bullets: [Laser.init(sprite: #imageLiteral(resourceName: "Laser"), width: 0.5, travelSpeed: 500, duration: 1.5, fade: 0.25, angle: 0)], rounds: 1, timeBetweenRounds: 0, aim: .player, angle: 0, angleChange: 0, sound: "laser01.m4a")
        let laserGun1 = Gun(delay: 3, reload: 4, volleys: .single(laserVolley1), position: .init(x: 0, y: -24))
        for i in 1...3 {
            let d = Double(i)
            let enemy = Enemy.init(priority: i, life: 30, delay: d * 0.2, sprite: #imageLiteral(resourceName: "BasicEnemy"))
            enemy.position = CGPoint(x: -600 + d * 300, y: 400)
            enemy.setMovement(start: [.init(x: 0, y: -300, speed: 200, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
            enemy.explosion = explode
            enemy.explodeSound = "explosion01.m4a"
            enemy.addGun(laserGun1)
            wave3.append(enemy)
        }
        
        for i in 4...5 {
            let d = Double(i)
            
            let enemy = Enemy.init(priority: i, life: 30, delay: d * 0.2, sprite: #imageLiteral(resourceName: "BasicEnemy"))
            enemy.position = CGPoint(x: -1350 + d * 300, y: 400)
            enemy.setMovement(start: [.init(x: 0, y: -250, speed: 200, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
            enemy.explosion = explode
            enemy.explodeSound = "explosion01.m4a"
            enemy.addGun(laserGun1)
            wave3.append(enemy)
        }
        
        waves.append(wave3)
        
        var wave4 = [Enemy]()
        
        let reset = SKAction.move(to: CGPoint.init(x: 0, y: 220), duration: 2)
        reset.timingMode = .easeOut
        
        
        let boss = Enemy(priority: 1, life: 300, delay: 0, sprite: #imageLiteral(resourceName: "LargeEnemy"))
        boss.color = .red
        boss.colorBlendFactor = 0
        boss.position = CGPoint(x: 0, y: 220)
        boss.alpha = 0
        boss.phaseIn = SKAction.fadeIn(withDuration: 2)
        boss.phaseOut = SKAction.group([reset, .colorize(withColorBlendFactor: 0.5, duration: 2)])
        
        

        let boolet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 150, angle: 0)
        let singleVolley = Volley(bullets: [boolet], rounds: 1, timeBetweenRounds: 0, aim: .player, angle: 0, angleChange: 0, sound: "shot01.m4a")
        let singleShot = Gun(delay: 0.5, reload: 0.5, volleys: .single(singleVolley), position: CGPoint(x: 0, y: -40))
        boolet.color = .cyan
        boolet.colorBlendFactor = 1
        

        var lasers = [Shootable]()
        for i in 1...2 {
            let d = Double(i)
            
            let laser = Laser.init(sprite: #imageLiteral(resourceName: "Laser"), width: 1, travelSpeed: 1000, duration: 2, fade: 0.25, angle: -60 + d * 40)
            lasers.append(laser)
        }
        let laserVolley2 = Volley(bullets: lasers, rounds: 1, timeBetweenRounds: 0, aim: .player, angle: 0, angleChange: 0, sound: "laser01.m4a")
        let laserGun2 = Gun(delay: 1, reload: 2, volleys: .single(laserVolley2) ,position: CGPoint.init(x: 0, y: 0))
        
        boss.addGun(singleShot)
        boss.addGun(laserGun2)
        boss.explodeSound = "longexplosion01.m4a"
        boss.isBoss = true
        boss.bossMusic = "Boss01.m4a"
        
        let boss1 = Enemy(priority: 1, life: 300, delay: 0, sprite: #imageLiteral(resourceName: "LargeEnemy"))
        boss.nextPhase = boss1
        boss1.color = .red
        boss1.colorBlendFactor = 0.5
        boss1.phaseOut = SKAction.group([reset, .colorize(withColorBlendFactor: 0.9, duration: 2)])
        boss1.isBoss = true

        var iLasers1 = [Shootable]()
        for i in 0...4 {
            let d = Double(i)
            
            let laser = InstantLaser.init(sprite: #imageLiteral(resourceName: "Laser"), angle: -40 + d * 20, delay: 0.5, duration: 1, width: 1)
            laser.sound = "laser01.m4a"
            iLasers1.append(laser)
        }
        let iLaserVolley1 = Volley(bullets: iLasers1, rounds: 1, timeBetweenRounds: 0, aim: .player, angle: 0, angleChange: nil, sound: nil)
        let instantLaserSpread = Gun(delay: 1, reload: 2, volleys: .single(iLaserVolley1) ,position: CGPoint.init(x: 0, y: -20))
        

        
        var iLasers2 = [Shootable]()
        for i in 0...3 {
            let d = Double(i)
            
            let laser = InstantLaser.init(sprite: #imageLiteral(resourceName: "Laser"), angle: -30 + d * 20, delay: 0.5, duration: 1, width: 1)
            laser.sound = "laser02.m4a"
            iLasers2.append(laser)
        }
        
        let iLaserVolley2 = Volley(bullets: iLasers2, rounds: 1, timeBetweenRounds: 0, aim: .player, angle: 0, angleChange: nil, sound: nil)
        let instantLaserSpread1 = Gun(delay: 2, reload: 2, volleys: .single(iLaserVolley2), position: CGPoint.init(x: 0, y: -20))
        
        boss1.addGun(instantLaserSpread)
        boss1.addGun(instantLaserSpread1)
        
        boss1.explodeSound = "longexplosion01.m4a"
        
        let boss2 = Enemy(priority: 1, life: 450, delay: 0, sprite: #imageLiteral(resourceName: "LargeEnemy"))
        boss1.nextPhase = boss2
        
        boss2.color = .red
        boss2.colorBlendFactor = 0.9
        
        let straightShot = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 300, angle: 0)
        let straightVolley = Volley(bullets: [straightShot], rounds: 3, timeBetweenRounds: 0, aim: .none, angle: 0, angleChange: nil, sound: "shot01.m4a")
        straightVolley.x = -100
        straightVolley.xChange = 100
        let straightVolleyGun = Gun.init(delay: 1, reload: 0.5, volleys: .single(straightVolley), position: CGPoint.init(x: 0, y: 0))
        boss2.addGun(straightVolleyGun)
        
        boss2.setMovement(start: nil, loop: [.init(x: 300, y: 0, speed: 150, moveType: .linear, speedType: .slowDown, delay: 0), .init(x: -300, y: 0, speed: 150, moveType: .linear, speedType: .speedUp, delay: 0), .init(x: -300, y: 0, speed: 150, moveType: .linear, speedType: .slowDown, delay: 0), .init(x: 300, y: 0, speed: 150, moveType: .linear, speedType: .speedUp, delay: 0)], remove: false)
        
        let spreadVolley = Volley(bullets: bullets1, rounds: 2, timeBetweenRounds: 0.5, aim: .player, angle: 0, angleChange: nil, sound: "shot01.m4a")
        let spreadGun2 = Gun(delay: 1.5, reload: 1, volleys: .single(spreadVolley), position: .init(x: 0, y: -64))
        
        boss2.addGun(spreadGun2)
        boss2.explosion = "bossExplosion"
        boss2.explodeSound = "longexplosion01.m4a"
        boss2.phaseOut = SKAction.fadeOut(withDuration: 1)
        
        boss2.isBoss = true
        
        wave4.append(boss)
        waves.append(wave4)
    }
    
}
