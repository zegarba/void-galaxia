//
//  TitleScreen.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 12/27/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

class TitleScreen: SKScene{
    
    var currentNode: SKLabelNode!
    var stageSelect: SKLabelNode!
    var selectButton: SKAction!
    var startFlash: SKAction!
    var start: UITapGestureRecognizer!
    var up: UISwipeGestureRecognizer!
    var down: UISwipeGestureRecognizer!
    var right: UISwipeGestureRecognizer!
    var left: UISwipeGestureRecognizer!
    
    var tutorialOpen = false
    
    var firstStart = true
    
    override func didMove(to view: SKView) {

        if firstStart {
            firstStart = false
        }
        else {
            MusicPlayer.fadeOut()
        }
        
        // make start button blink
        currentNode = childNode(withName: "start") as! SKLabelNode
        currentNode.color = .yellow
        startFlash = SKAction.repeatForever(SKAction.sequence([SKAction.hide(), SKAction.wait(forDuration: 0.25), SKAction.unhide(), SKAction.wait(forDuration: 0.25)]))
        currentNode.run(startFlash)
        currentNode.fontColor = .yellow
        
        //create gesture recognizer to start game
        start = UITapGestureRecognizer(target: self, action: #selector(selectOption))
        start.allowedPressTypes = [NSNumber(value: UIPressType.select.hashValue)]
        view.addGestureRecognizer(start)
        
        up = UISwipeGestureRecognizer(target: self, action: #selector(moveUp))
        up.direction = .right
        view.addGestureRecognizer(up)
        
        down = UISwipeGestureRecognizer(target: self, action: #selector(moveDown))
        down.direction = .left
        view.addGestureRecognizer(down)
        
        right = UISwipeGestureRecognizer(target: self, action: #selector(moveRight))
        right.direction = .down
        view.addGestureRecognizer(right)
        
        left = UISwipeGestureRecognizer(target: self, action: #selector(moveLeft))
        left.direction = .up
        view.addGestureRecognizer(left)
        
        //stars!
        let stars = SKEmitterNode(fileNamed: "stage1Background")!
        stars.position = CGPoint(x: 0, y: size.height * 0.5)
        stars.zPosition = -1
        stars.name = "stars"
        addChild(stars)
        stars.advanceSimulationTime(5)
    }
    
    @objc func moveUp(){
        currentNode.removeAllActions()
        currentNode.run(.unhide())
        currentNode.fontColor = .white
        if currentNode.name == "select" {
            for child in children {
                if child.name == "1" || child.name == "2" || child.name == "3"{
                    (child as! SKLabelNode).fontColor = .white
                    child.removeAllActions()
                    child.run(.hide())
                }
            }
            currentNode.text = "Stage Select"
        }
        switch currentNode.name! {
        case "start":
            currentNode = childNode(withName: "tutorial") as! SKLabelNode
        case "select":
            currentNode = childNode(withName: "start") as! SKLabelNode
        default:
            currentNode = childNode(withName: "select") as! SKLabelNode
            for child in children {
                if child.name == "1" || child.name == "2" || child.name == "3" {
                    child.run(.unhide())
                }

            }
            stageSelect = childNode(withName: "1") as! SKLabelNode
            stageSelect.fontColor = .yellow
        }
        
        currentNode.run(startFlash)
        currentNode.fontColor = .yellow
        if currentNode.name == "select" {
            currentNode.text = "Stage Select:"
        }
    }
    
    @objc func moveDown(){
        currentNode.removeAllActions()
        currentNode.run(.unhide())
        currentNode.fontColor = .white
        
        if currentNode.name == "select" {
            for child in children {
                if child.name == "1" || child.name == "2" || child.name == "3"{
                    child.removeAllActions()
                    child.run(.hide())
                    (child as! SKLabelNode).fontColor = .white
                }
            }
            currentNode.text = "Stage Select"
        }
        
        switch currentNode.name! {
        case "start":
            currentNode = childNode(withName: "select") as! SKLabelNode
            for child in children {
                if child.name == "1" || child.name == "2" || child.name == "3" {
                    child.run(.unhide())
                }
            }
            stageSelect = childNode(withName: "1") as! SKLabelNode
            stageSelect.fontColor = .yellow
        case "select":
            currentNode = childNode(withName: "tutorial") as! SKLabelNode
        default:
            currentNode = childNode(withName: "start") as! SKLabelNode
        }
        
        currentNode.run(startFlash)
        currentNode.fontColor = .yellow

        if currentNode.name == "select" {
            currentNode.text = "Stage Select:"
        }
        
    }
    
    @objc func moveRight(){
        // do nothing if select isn't currently highlighted
        if currentNode.name != "select" {return}
        
        stageSelect.removeAllActions()
        stageSelect.fontColor = .white
        switch stageSelect.name! {
        case "1":
            stageSelect = childNode(withName: "2") as! SKLabelNode
        case "2":
            stageSelect = childNode(withName: "3") as! SKLabelNode
        default:
            stageSelect = childNode(withName: "1") as! SKLabelNode
        }
        
        stageSelect.fontColor = .yellow
        
    }
    
    @objc func moveLeft(){
        // do nothing if select isn't currently highlighted
        if currentNode.name != "select" {return}
        
        stageSelect.removeAllActions()
        stageSelect.fontColor = .white
        switch stageSelect.name! {
        case "1":
            stageSelect = childNode(withName: "3") as! SKLabelNode
        case "2":
            stageSelect = childNode(withName: "1") as! SKLabelNode
        default:
            stageSelect = childNode(withName: "2") as! SKLabelNode
        }
        
        stageSelect.fontColor = .yellow
        
        
    }
    
    @objc func selectOption(){
        
        // if the tutorial is running turn it off.
        if tutorialOpen {
            tutorialOpen = false
            view?.viewWithTag(10)?.removeFromSuperview()
            view?.gestureRecognizers?.forEach { gesture in
                gesture.isEnabled = true
                
            }
            alpha = 1
            currentNode.run(startFlash)
            return
            
        }
        
        let confirm = SKAction.playSoundFileNamed("confirm01.m4a", waitForCompletion: false)
        run(confirm)
        
        // make button blink faster then load game scene
        
        currentNode.removeAllActions()
        currentNode.run(.repeat(.sequence([.hide(), .wait(forDuration: 0.1),.unhide(), .wait(forDuration: 0.1)]), count: 4)){
        switch self.currentNode.name! {
        case "start":
            self.startGame()
        case "select":
            self.selectStage()
        default:
            self.startTutorial()
            }
        }
    }
    
    func startGame(){
        
        

        // remove start button gesture
        view!.gestureRecognizers?.forEach(view!.removeGestureRecognizer)
        

        
        let game = GameScene.getStage(1)
        let transition = SKTransition.fade(withDuration: 1)
        MusicPlayer.selectTrack(1)
        self.view!.presentScene(game, transition: transition)
        MusicPlayer.stop()
    }
    
    func selectStage(){
        
        
        // remove start button gesture
        view!.gestureRecognizers?.forEach(view!.removeGestureRecognizer)
        
        let game = GameScene.getStage(Int(stageSelect.name!)!)
        MusicPlayer.selectTrack(Int(stageSelect.name!)!)
        let transition = SKTransition.fade(withDuration: 1)
        self.view!.presentScene(game, transition: transition)
    }
    
    override func willMove(from view: SKView) {
        MusicPlayer.play()
    }
    
    func startTutorial(){
        
        tutorialOpen = true
        
        view?.gestureRecognizers?.forEach { gesture in
            gesture.isEnabled = false
        }
        alpha = 0.5
        start.isEnabled = true
        let tutView = SKView.init(frame: .init(x: 0, y: 0, width: 1200, height: 800))
        let tutScene = SKScene.init(fileNamed: "Tutorial.sks")
        tutScene?.scaleMode = .aspectFill
        tutView.tag = 10
        tutView.center = view!.center
        tutView.presentScene(tutScene)
        view?.addSubview(tutView)
        
    }
    
}
