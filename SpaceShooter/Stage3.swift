//
//  Stage3.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 2/14/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

extension GameScene {
    
    func stage3() {
        
        stageNo = 3
        
        // set background stars
        let stars = SKEmitterNode(fileNamed: "stage3Background1.sks")!
        let backgroundShots = SKEmitterNode(fileNamed: "stage3Background2.sks")!
        let backgroundShots2 = SKEmitterNode(fileNamed: "stage3Background2b.sks")!
        backgroundShots.particleSize = CGSize.init(width: 4, height: 15)
        backgroundShots2.particleSize = CGSize.init(width: 4, height: 15)
        
        let background3 = SKEmitterNode(fileNamed: "stage3Background3.sks")!
        let background4 = SKEmitterNode(fileNamed: "stage3Background4.sks")!
        
        background3.position = .init(x: 0, y: size.height * 0.5)
        background4.position = .init(x: 0, y: size.height * -0.5)
        
        addChild(stars)
        addChild(backgroundShots)
        addChild(backgroundShots2)
        addChild(background3)
        addChild(background4)
        
        func wave1() {
            var wave = [Enemy]()
            
            let circle = Bullet.init(sprite: #imageLiteral(resourceName: "CircleBullet"), speed: 200, angle: 0)
            circle.angleRand = 1.5
            circle.moveRand = 60
            let burstVolley = Volley(bullets: [circle], rounds: 3, timeBetweenRounds: 0, aim: .player)
            burstVolley.angle = -3
            burstVolley.angleChange = 3
            burstVolley.sound = "shot01.m4a"
            
            let burstGun1 = Gun(delay: 0.75, reload: 50, volleys: .single(burstVolley), position: .init(x: 0, y: 0))
            let burstGun2 = Gun(delay: 1, reload: 50, volleys: .single(burstVolley), position: .init(x: 0, y: 0))
            let burstGun3 = Gun(delay: 1.25, reload: 50, volleys: .single(burstVolley), position: .init(x: 0, y: 0))
            
            burstGun1.delayRand = 0.5
            burstGun2.delayRand = 0.5
            burstGun3.delayRand = 0.5
            for i in 0...2{
                for j in 0...4 {
                    let enemy = Enemy(priority: j, life: 15, delay: 0.2 * j.double + 2.3 * i.double, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
                    
                    enemy.explodeSound = "explosion01.m4a"
                    enemy.explosion = "explosion.sks"
                    enemy.color = .red
                    enemy.colorBlendFactor = 0.75
                    
                    let speed = 350.cgfloat
                    switch i {
                    case 0:
                        enemy.position = .init(x: -250, y: 325)
                        enemy.spin(0.75)
                        enemy.setMovement(start: [.init(x: 0, y: -100, speed: speed, moveType: .linear, speedType: .constant, delay: 0), .init(x: 250, y: -100, speed: speed, moveType: .yCurve, speedType: .constant, delay: 0), .init(x: 250, y: 100, speed: speed, moveType: .xCurve, speedType: .constant, delay: 0), .init(x: 0, y: 100, speed: speed, moveType: .linear, speedType: .constant, delay: 0)], loop: nil, remove: true)
                        enemy.addGun(burstGun1)
                    case 1:
                        enemy.position = .init(x: 300, y: 325)
                        enemy.spin(-0.75)
                        enemy.setMovement(start: [.init(x: 0, y: -125, speed: speed, moveType: .linear, speedType: .constant, delay: 0), .init(x: -300, y: -125, speed: speed, moveType: .yCurve, speedType: .constant, delay: 0), .init(x: -300, y: 125, speed: speed, moveType: .xCurve, speedType: .constant, delay: 0), .init(x: 0, y: 125, speed: speed, moveType: .linear, speedType: .constant, delay: 0)], loop: nil, remove: true)
                        enemy.addGun(burstGun2)
                    default:
                        enemy.position = .init(x: -350, y: 325)
                        enemy.spin(0.75)
                        enemy.setMovement(start: [.init(x: 0, y: -150, speed: speed, moveType: .linear, speedType: .constant, delay: 0), .init(x: 350, y: -150  , speed: speed, moveType: .yCurve, speedType: .constant, delay: 0), .init(x: 350, y: 150, speed: speed, moveType: .xCurve, speedType: .constant, delay: 0), .init(x: 0, y: 150, speed: speed, moveType: .linear, speedType: .constant, delay: 0)], loop: nil, remove: true)
                        enemy.addGun(burstGun3)
                    }
                    wave.append(enemy)
                }
            }
            
            waves.append(wave)
        }

        func wave2() {
            var wave = [Enemy]()
            
            let machBullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 250, angle: 0)
            machBullet.angleRand = 25
            machBullet.color = .blue
            machBullet.colorBlendFactor = 0.55
            let machVolley = Volley.init(bullets: [machBullet], rounds: 10, timeBetweenRounds: 0.2, aim: .player)
            machVolley.sound = "shot01.m4a"
            let machGun = Gun.init(delay: 1, reload: 2, volleys: .single(machVolley), position: .init(x: 0, y: -64))
            for i in 0...1{
                let machEnemy = Enemy.init(priority: 1, life: 75, delay: 1 + i.double * 0.4, sprite: #imageLiteral(resourceName: "LargeEnemy"))
                machEnemy.position = .init(x: -300 + i.cgfloat * 600, y: 350)
                machEnemy.color = .blue
                machEnemy.colorBlendFactor = 0.55
                machEnemy.addGun(machGun)
                machEnemy.setMovement(start: [.init(x: 0, y: -150, time: 1, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
                machEnemy.explosion = "explosion01.sks"
                machEnemy.explodeSound = "explosion03.m4a"
                wave.append(machEnemy)
            }
            
            let spreadBullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 150, angle: 0)
            spreadBullet.color = .red
            spreadBullet.colorBlendFactor = 0.4
            let spreadVolley = Volley.init(bullets: [spreadBullet], rounds: 3, timeBetweenRounds: 0, aim: .player)
            spreadVolley.angle = -15
            spreadVolley.angleChange = 15
            spreadVolley.sound = "shot02.m4a"
            let spreadGun = Gun.init(delay: 2, reload: 1.5, volleys: .single(spreadVolley), position: .init(x: 0, y: -24))
            
            for i in 0...1{
                let spreadEnemy = Enemy(priority: 1, life: 45, delay: 0.5 + i.double * 0.3, sprite: #imageLiteral(resourceName: "BasicEnemy"))
                spreadEnemy.colorBlendFactor = 0.3
                spreadEnemy.color = .red
                spreadEnemy.position = .init(x: -200 + i.cgfloat * 400, y: 325)
                spreadEnemy.setMovement(start: [.init(x: 0, y: -225, time: 1, moveType: .linear, speedType: .constant, delay: 0)], loop: nil, remove: false)
                spreadEnemy.explosion = "explosion.sks"
                spreadEnemy.explodeSound = "explosion01.m4a"
                spreadEnemy.addGun(spreadGun)
                wave.append(spreadEnemy)
            }
            
            let bomb = Bullet.init(sprite: #imageLiteral(resourceName: "CircleBullet"), speed: 200, angle: 0)
            bomb.acceleration = -200
            bomb.duration = 1
            bomb.setScale(3)
            bomb.color = .yellow
            bomb.colorBlendFactor = 0.75
            
            var bombShots = [Bullet]()
            
            for i in 0...11 {
            
                let bombShot = Bullet.init(sprite: #imageLiteral(resourceName: "CircleBullet"), speed: 150, angle: 0)
                bombShot.colorBlendFactor = 0.75
                bombShot.color = .yellow
                bombShot.fallSpeed = 145
                bombShot.fallRand = 10
                bombShot.angleRand = 5
                bombShot.angle = 90 + 15 * i.double
                bombShots.append(bombShot)
            }
            
            let bombVolley = Volley.init(bullets: bombShots, rounds: 1, timeBetweenRounds: 0, aim: .none)
            bombVolley.sound = "explosion01.m4a"
            
            bomb.setNextStep(.single(bombVolley))
            
            let singleBomb = Volley.init(bullets: [bomb], rounds: 1, timeBetweenRounds: 0, aim: .none)
            singleBomb.sound = "explosion02.m4a"
            
            let bombEnemy = Enemy.init(priority: 1, life: 100, delay: 1.6, sprite: #imageLiteral(resourceName: "LargeEnemy02"))
            bombEnemy.position = .init(x: 0, y: 350)
            bombEnemy.setMovement(start: [.init(x: 0, y: -150, time: 1, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
            bombEnemy.explosion = "explosion.sks"
            bombEnemy.explodeSound = "explosion02.m4a"
            for i in 0...1 {
                let bombGun = Gun.init(delay: 1.8, reload: 3, volleys: .single(singleBomb), position: .init(x: -28 + i.cgfloat * 56, y: -68))
                bombEnemy.addGun(bombGun)
            }
            wave.append(bombEnemy)
            
            waves.append(wave)
        }

        func wave3() {
            var wave = [Enemy]()
            
            var spinBullets = [Bullet]()
            for i in 0...7{
                let spinBullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 75, angle: i.double * 360.0/8.0)
                spinBullet.color = .red
                spinBullet.colorBlendFactor = 0.35
                spinBullets.append(spinBullet)
            }
            let clock = Volley.init(bullets: spinBullets, rounds: 4, timeBetweenRounds: 1.2, aim: .none)
            clock.angleChange = 90.0/4.0
            clock.sound = "shot01.m4a"
            let counter = Volley.init(bullets: spinBullets, rounds: 4, timeBetweenRounds: 1.2, aim: .none)
            counter.angleChange = -90.0/4.0
            counter.sound = "shot01.m4a"
            let clockGun = Gun.init(delay: 1, reload: 1, volleys: .single(clock), position: .init())
            let counterGun = Gun.init(delay: 1, reload: 1, volleys: .single(counter), position: .init())
            
            for i in 0...1{
                let enemy1 = Enemy.init(priority: 1, life: 150, delay: 0.5 + i.double * 0.8, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
                enemy1.spin(1/4)
                enemy1.position = .init(x: -350 + i.cgfloat * 100, y: 350)
                enemy1.setMovement(start: [.init(x: 0, y: -150, time: 1, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
                enemy1.addGun(clockGun)
                enemy1.explosion = "explosion.sks"
                enemy1.explodeSound = "explosion01.m4a"
                
                let enemy2 = Enemy.init(priority: 1, life: 150, delay: 2.1 + i.double * 0.8, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
                enemy2.spin(-1/4)
                enemy2.position = .init(x: 350 - i.cgfloat * 100, y: 350)
                enemy2.setMovement(start: [.init(x: 0, y: -150, time: 1, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
                enemy2.addGun(counterGun)
                enemy2.explosion = "explosion.sks"
                enemy2.explodeSound = "explosion01.m4a"
                
                wave.append(enemy1)
                wave.append(enemy2)
                
            }
            
            let large = Enemy.init(priority: 1, life: 200, delay: 3.4, sprite: #imageLiteral(resourceName: "LargeEnemy02"))
            large.position = .init(x: 0, y: 350)
            large.setMovement(start: [.init(x: 0, y: -200, time: 1, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
            large.explosion = "explosion.sks"
            large.explodeSound = "explosion02.m4a"
            
            let machBullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 125, angle: 0)
            machBullet.angleRand = 15
            
            let machVolley = Volley.init(bullets: [machBullet], rounds: 10, timeBetweenRounds: 0.6, aim: .player)
            machVolley.sound = "shot02.m4a"
            for i in 0...1 {
                let gun = Gun(delay: 1, reload: 3, volleys: .single(machVolley), position: .init(x: -28 + i.double * 56, y: -68))
                large.addGun(gun)
            }
            wave.append(large)
            waves.append(wave)
        }

        func wave4() {
            var wave = [Enemy]()
            
            let laser = Laser.init(sprite: #imageLiteral(resourceName: "Laser"), width: 1, travelSpeed: 600, duration: 2, fade: 0.5, angle: 0)
            let laserVolley = Volley.init(bullets: [laser], rounds: 1, timeBetweenRounds: 0, aim: .player)
            laserVolley.sound = "laser01.m4a"
            let laserGun = Gun.init(delay: 0.75, reload: 20, volleys: .single(laserVolley), position: .init())
            for i in 0...5 {
                let enemy1 = Enemy.init(priority: i, life: 6, delay: 5.4 + i.double * 0.8, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
                enemy1.explosion = "explosion.sks"
                enemy1.explodeSound = "explosion01.m4a"
                enemy1.spin(1.5)
                enemy1.position = .init(x: -300, y: 300)
                enemy1.setMovement(start: [.init(x: 800, y: -350, time: 2.5, moveType: .linear, speedType: .constant, delay: 0)], loop: nil, remove: true)
                enemy1.addGun(laserGun)
                wave.append(enemy1)
                
                let enemy2 = Enemy.init(priority: i, life: 6, delay: 5.8 + i.double * 0.8, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
                enemy2.explosion = "explosion.sks"
                enemy2.explodeSound = "explosion01.m4a"
                enemy2.spin(-1.5)
                enemy2.position = .init(x: 300, y: 300)
                enemy2.setMovement(start: [.init(x: -800, y: -350, time: 2.5, moveType: .linear, speedType: .constant, delay: 0)], loop: nil, remove: true)
                enemy2.addGun(laserGun)
                wave.append(enemy2)
            }
            
            waves.append(wave)
            
            var wave2 = [Enemy]()
            for enemy in wave{
                let copy = enemy.copy() as! Enemy
                copy.delay = copy.delay - 4
                wave2.append(copy)
                
            }
            
            waves.append(wave2)
        }
        
        func wave5() {
            var wave = [Enemy]()
            
            var spread = [Bullet]()
            for i in 0...2 {
                let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 150, angle: -25 + i.double * 25)
                bullet.color = .cyan
                bullet.colorBlendFactor = 0.68
                spread.append(bullet)
            }
            let volley = Volley.init(bullets: spread, rounds: 5, timeBetweenRounds: 0.15, aim: .playerFreeze)
            volley.sound = "shot01.m4a"
            volley.angleRand = 5
            let gun1 = Gun.init(delay: 0.5, reload: 2.5, volleys: .single(volley), position: .init(x: -28, y: -68))
            let gun2 = Gun.init(delay: 0.5, reload: 2.5, volleys: .single(volley), position: .init(x: 28, y: -68))
            
            for i in 0...1 {
                let enemy = Enemy.init(priority: 1, life: 300, delay: 4 + i.double * 0.01, sprite: #imageLiteral(resourceName: "LargeEnemy02"))
                enemy.position = .init(x: -200 + i.cgfloat * 400, y: 300)
                enemy.setMovement(start: [.init(x: 0, y: -100, time: 0.8, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
                enemy.explodeSound = "explosion01.m4a"
                enemy.explosion = "explosion.sks"
                enemy.addGuns([gun1, gun2])
                wave.append(enemy)
                
            }
            
            
            waves.append(wave)
        }
        
        func wave6() {
            var wave = [Enemy]()
            
            var spread = [Bullet]()
            var circle = [Bullet]()
            
            for _ in 0...0 {
                let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 150, angle: 0)
                bullet.angleRand = 50
                spread.append(bullet)
            }
            
            let spreadVolley = Volley.init(bullets: spread, rounds: 1, timeBetweenRounds: 0, aim: .player)
            
            for i in 0...23 {
                let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "CircleBullet"), speed: 100, angle: i.double * 15)
                bullet.acceleration = -50
                bullet.duration = 2
                let volley = Volley(spreadVolley)
                if i == 0 {
                    volley.sound = "shot02.m4a"
                }
                bullet.setNextStep(.single(volley))
                circle.append(bullet)
            }
            
            let circleVolley = Volley.init(bullets: circle, rounds: 3, timeBetweenRounds: 1, aim: .none)
            circleVolley.sound = "shot01.m4a"
            
            let enemy = Enemy.init(priority: 1, life: 350, delay: 1, sprite: #imageLiteral(resourceName: "LargeEnemy"))
            enemy.position = .init(x: 250, y: 310)
            enemy.setMovement(start: [.init(x: 0, y: -200, time: 2, moveType: .linear, speedType: .slowDown, delay: 0)], loop: [.init(x: -500, y: 0, time: 4, moveType: .linear, speedType: .speedUpSlowDown, delay: 2), .init(x: 500, y: 0, time: 4, moveType: .linear, speedType: .speedUpSlowDown, delay: 2)], remove: false)
            enemy.explodeSound = "explosion03.m4a"
            enemy.explosion = "explosion02.sks"
            
            let gun = Gun.init(delay: 2, reload: 4, volleys: .single(circleVolley), position: .init(x: 0, y: -64))
            
            enemy.addGun(gun)
            wave.append(enemy)
            
            waves.append(wave)
        }
        
        
        func bossWave() {
            let shake: CGFloat = 3
            var wave = [Enemy]()
            let boss = Enemy.init(priority: 1, life: 500, delay: 1, sprite: #imageLiteral(resourceName: "Boss03-1"))
            let height = boss.size.height
            boss.position = .init(x: 0, y: 288 + height / 2)
            boss.phaseIn = SKAction.moveBy(x: 0, y: -height, duration: 5)
            boss.phaseIn?.timingMode = .easeOut
            boss.isBoss = true
            boss.bossMusic = "Boss02.m4a"
            boss.phaseOut = .wait(forDuration: 3)
            boss.explosion = "boss03explosion01.sks"
            boss.explodeSound = "longexplosion01.m4a"
            boss.explode?.position = .init(x: -300, y: 60)
            
            let boss1 = Enemy.init(priority: 1, life: 500, delay: 0, sprite: #imageLiteral(resourceName: "Boss03-2"))
            boss1.isBoss = true
            boss.nextPhase = boss1
            boss1.explosion = "boss03explosion02.sks"
            boss1.explodeSound = "explosion04.m4a"
            boss1.explode?.position = .init(x: -50, y: 0)
            boss1.phaseOut = .repeat(.sequence([.moveBy(x: shake, y: 0, duration: 0.1), .moveBy(x:-shake, y: 0, duration: 0.1)]), count: 20)
            
            
            let boss2 = Enemy.init(priority: 1, life: 500, delay: 0, sprite: #imageLiteral(resourceName: "Boss03-3"))
            boss1.nextPhase = boss2
            boss2.isBoss = true
            boss2.explodeSound = "longexplosion01.m4a"
            boss2.explosion = "boss03explosion03.sks"
            boss2.explode?.position = .init(x: 300, y: 40)
            boss2.phaseOut = .wait(forDuration: 3)
            
            
            let boss3 = Enemy.init(priority: 1, life: 500, delay: 0, sprite: #imageLiteral(resourceName: "Boss03-4"))
            boss2.nextPhase = boss3
            boss3.isBoss = true
            boss3.explodeSound = "explosion04.m4a"
            boss3.explosion = "boss03explosion04.sks"
            boss3.explode?.position = .init(x: 50, y: 0)
            boss3.phaseOut = .repeat(.sequence([.moveBy(x: shake, y: 0, duration: 0.1), .moveBy(x: -shake, y: 0, duration: 0.1)]), count: 20)
            
            let boss4 = Enemy.init(priority: 1, life: 1500, delay: 0, sprite: #imageLiteral(resourceName: "Boss03-5"))
            boss3.nextPhase = boss4
            boss4.isBoss = true
            boss4.bossMusic = "Boss02 Final.m4a"
            boss4.explodeSound = "boss03explosion.m4a"
            boss4.explosion = "boss03explosion05.sks"
            boss4.explode?.position = .init(x: -100, y: 0)
            boss4.phaseOut = .sequence([.colorize(with: .red, colorBlendFactor: 0.8, duration: 2.5), .fadeOut(withDuration: 2.5)])
            
            let laser = Laser.init(sprite: #imageLiteral(resourceName: "Laser"), width: 2, travelSpeed: 200, duration: 4, fade: 1, angle: 0)
            laser.color = .yellow
            laser.colorBlendFactor = 1
            let laserVolley = Volley.init(bullets: [laser], rounds: 1, timeBetweenRounds: 0, aim: .player)
            laserVolley.sound = "laser01.m4a"
            
            for i in 0...1 {
                let gun = Gun.init(delay: 2 + i.double * 0.4, reload: 6, volleys: .single(laserVolley), position: .init(x: -380 + i * 380 * 2, y: -14))
                boss.addGun(gun)
                let gun1 = Gun.init(delay: 2.8 + i.double * 0.4, reload: 6, volleys: .single(laserVolley), position: .init(x: -285 + 285 * 2 * i, y: -62))
                boss.addGun(gun1)
                let gun2 = Gun.init(delay: 3.6 + i.double * 0.4, reload: 6, volleys: .single(laserVolley), position: .init(x: -161 + i * 161 * 2, y: -106))
                boss.addGun(gun2)
                
                if i == 1 {
                    boss1.addGuns([gun, gun1, gun2])
                }
            }
            
            var lasers1 = [Laser]()
            for i in 0...8 {
                let laser = Laser.init(sprite: #imageLiteral(resourceName: "Laser"), width: 5, travelSpeed: 300, duration: 5, fade: 3, angle: 70 + i.double * -10)
                laser.color = .yellow
                laser.colorBlendFactor = 1
                lasers1.append(laser)
            }
            let laserVolley1 = Volley.init(bullets: lasers1, rounds: 1, timeBetweenRounds: 0, aim: .none)
            laserVolley1.sound = "laser01.m4a"
            let laserGun1 = Gun.init(delay: 2, reload: 6, volleys: .single(laserVolley1), position: .init(x: -161, y: -106))
            boss1.addGun(laserGun1)
            
            var bullets = [Bullet]()
            for i in 0...2 {
                let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 450, angle: -20 + i.double * 20)
                bullets.append(bullet)
            }
            
            let spreadVolley = Volley.init(bullets: bullets, rounds: 2, timeBetweenRounds: 2, aim: .player)
            spreadVolley.sound = "shot01.m4a"
            
            for i in 0...1 {
                let gun1 = Gun.init(delay: 1 + i.double * 0.5, reload: 3, volleys: .single(spreadVolley), position: .init(x: -331.5 + i.double * 2 * 331.5, y: 38))
                boss.addGun(gun1)
                let gun2 = Gun.init(delay: 2 + i.double * 0.5, reload: 3, volleys: .single(spreadVolley), position: .init(x: -188 + i.double * 2 * 188, y: -55))
                boss.addGun(gun2)
            }
            
            let machVolley = Volley.init(bullets: bullets, rounds: 3, timeBetweenRounds: 0.2, aim: .player)
            machVolley.sound = "shot01.m4a"
            
            do {
                let gun1 = Gun.init(delay: 1, reload: 3, volleys: .single(machVolley), position: .init(x: 331.5, y: 38))
                boss1.addGun(gun1)
                let gun2 = Gun.init(delay: 2, reload: 3, volleys: .single(machVolley), position: .init(x: 188, y: -55))
                boss1.addGun(gun2)
            }
            
            let waveBullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 250, angle: 0)
            waveBullet.color = .yellow
            waveBullet.colorBlendFactor = 1
            let waveVolley = Volley.init(bullets: [waveBullet], rounds: 9, timeBetweenRounds: 0.1, aim: .playerFreeze)
            waveVolley.postDelay = 1
            waveVolley.angle = -40
            waveVolley.angleChange = 10
            waveVolley.sound = "shot01.m4a"
            
            let waveGun1 = Gun.init(delay: 1, reload: 3, volleys: .sequence([waveVolley, waveVolley, waveVolley]), position: .init(x: 380, y: -14))
            let waveGun2 = Gun.init(delay: 1, reload: 3, volleys: .sequence([waveVolley, waveVolley, waveVolley]), position: .init(x: 285, y: -62))
            let waveGun3 = Gun.init(delay: 1, reload: 3, volleys: .sequence([waveVolley, waveVolley, waveVolley]), position: .init(x: 161, y: -106))
            
            boss2.addGuns([waveGun1, waveGun2, waveGun3])
            
            var bombWave = [Bullet]()
            for i in 0...3 {
                let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "CircleBullet"), speed: 100, angle: -15 + i.double * 10)
                bullet.color = .red
                bullet.colorBlendFactor = 1
                bombWave.append(bullet)
            }
            let bombWaveVolley = Volley.init(bullets: bombWave, rounds: 1, timeBetweenRounds: 0, aim: .previous)
            bombWaveVolley.sound = "shot01.m4a"
            let bomb = Bullet.init(sprite: #imageLiteral(resourceName: "CircleBullet"), speed: 200, angle: 0)
            bomb.colorBlendFactor = 1
            bomb.color = .red
            bomb.setScale(3)
            bomb.acceleration = -600
            bomb.duration = 0.5
            bomb.setNextStep(.single(bombWaveVolley))
            let bombVolley = Volley.init(bullets: [bomb], rounds: 1, timeBetweenRounds: 0, aim: .player)
            bombVolley.sound = "shot02.m4a"
            
            let bombGun1 = Gun.init(delay: 2, reload: 5, volleys: .single(bombVolley), position: .init(x: 331.5, y: 38))
            let bombGun2 = Gun.init(delay: 2, reload: 5, volleys: .single(bombVolley), position: .init(x: 188, y: -55))
            
            boss2.addGuns([bombGun1, bombGun2])
            
            let singleshot = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 200, angle: 0)
            singleshot.color = .yellow
            singleshot.colorBlendFactor = 1
            singleshot.angleRand = 70
            let singleVolley = Volley.init(bullets: [singleshot, singleshot], rounds: 10, timeBetweenRounds: 0.1, aim: .player)
            singleVolley.sound = "shot01.m4a"
            let singleGun = Gun.init(delay: 2, reload: 0.1, volleys: .single(singleVolley), position: .init(x: 161, y: -106))
            boss3.addGun(singleGun)
            
            let finalShot = Bullet.init(sprite: #imageLiteral(resourceName: "CircleBullet"), speed: 120, angle: 0)
            finalShot.color = .yellow
            finalShot.colorBlendFactor = 1
            finalShot.setScale(5)
            finalShot.duration = 1.5
            finalShot.acceleration = -150
            
            var finalWave = [[Volley]]()
            
            let straightShot = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 200, angle: 0)
            straightShot.color = .yellow
            straightShot.colorBlendFactor = 1
            
            let volley1 = Volley.init(bullets: [singleshot], rounds: 90, timeBetweenRounds: 0.05, aim: .none)
            volley1.postDelay = 0.4
            volley1.sound = "shot01.m4a"
            finalWave.append([volley1])
            let leftWave1 = Volley.init(bullets: [straightShot], rounds: 30, timeBetweenRounds: 0.07, aim: .none)
            let rightWave1 = Volley.init(bullets: [straightShot], rounds: 30, timeBetweenRounds: 0.07, aim: .none)
            leftWave1.angleChange = 50/30
            rightWave1.angleChange = -50/30
            
            rightWave1.postDelay = 0.6
            
            let leftWave2 = Volley.init(bullets: [straightShot], rounds: 30, timeBetweenRounds: 0.07, aim: .none)
            let rightWave2 = Volley.init(bullets: [straightShot], rounds: 30, timeBetweenRounds: 0.07, aim: .none)
            leftWave2.angle = 75
            leftWave2.angleChange = -60/30
            rightWave2.angleChange = 60/30
            rightWave2.angle = -75
            rightWave2.postDelay = 0.5
            
            leftWave1.sound = "shot01.m4a"
            rightWave1.sound = "shot01.m4a"
            leftWave2.sound = "shot01.m4a"
            rightWave2.sound = "shot01.m4a"
            
            finalWave.append([leftWave1, rightWave1])
            finalWave.append([leftWave2, rightWave2])
            
            finalWave.append([leftWave1, rightWave1])
            finalWave.append([leftWave2, rightWave2])
            
            finalWave.append([leftWave1, rightWave1])
            finalWave.append([leftWave2, rightWave2])
            
            finalWave.append([volley1])
            
            let bombF = Bullet.init(sprite: #imageLiteral(resourceName: "CircleBullet"), speed: 200, angle: 0)
            bombF.angleRand = 360
            let bombVolleyF = Volley.init(bullets: [straightShot], rounds: 5, timeBetweenRounds: 0, aim: .playerFreeze)
            bombVolleyF.angle = -45
            bombVolleyF.angleChange = 90 / 4
            bombVolleyF.sound = "shot02.m4a"
            bombF.setNextStep(.single(bombVolleyF))
            bombF.acceleration = -400
            bombF.duration = 0.5
            bombF.color = .yellow
            bombF.colorBlendFactor = 1
            
            let bombVolleyF2 = Volley.init(bullets: [bombF], rounds: 6, timeBetweenRounds: 0.8, aim: .none)
            bombVolleyF2.sound = "shot01.m4a"
            bombVolleyF2.postDelay = 1.75
            
            finalWave.append([bombVolleyF2])
            
            
            finalWave.append([leftWave1, rightWave1])
            finalWave.append([leftWave2, rightWave2])
            
            finalWave.append([bombVolleyF2, volley1])
            finalWave.append([bombVolleyF2, volley1])
            
            
            let iLaser = InstantLaser.init(sprite: #imageLiteral(resourceName: "Laser"), angle: 0, delay: 1, duration: 0.8, width: 2)
            iLaser.color = .yellow
            iLaser.colorBlendFactor = 1
            let iLaserVolley = Volley.init(bullets: [iLaser], rounds: 8, timeBetweenRounds: 0.4, aim: .none)
            iLaserVolley.sound = "laser01.m4a"
            
            let right1 = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 200, angle: 90)
            let right2 = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 200, angle: 90)
            let left1 = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 200, angle: -90)
            let left2 = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 200, angle: -90)
            
            right1.duration = 0
            left1.duration = 0
            right2.duration = 0.2
            left2.duration = 0.2
            
            for bullet in [right1, right2, left1, left2] {
                bullet.color = .yellow
                bullet.colorBlendFactor = 1
                bullet.setNextStep(.single(iLaserVolley))
            }
            
            let iLasers1 = Volley.init(bullets: [left1, right1], rounds: 1, timeBetweenRounds: 0, aim: .none)
            let iLasers2 = Volley.init(bullets: [left2, right2], rounds: 1, timeBetweenRounds: 0, aim: .none)
            
            for vol in [iLasers1, iLasers2]{
                vol.sound = "shot01.m4a"
                vol.postDelay = 2
            }
            
            finalWave.append([iLasers1])
            finalWave.append([iLasers2])
            
            finalWave.append([iLasers1])
            finalWave.append([iLasers2])
            
            finalWave.append([iLasers1])
            finalWave.append([iLasers2])
            
            finalWave.append([iLasers1])
            finalWave.append([iLasers2])
            
            var finalBlast = [Bullet]()
            var finalBlastWave = [Laser]()
            
            for i in 0...10 {
                let bullet = Laser.init(sprite: #imageLiteral(resourceName: "Laser"), width: 1.6, travelSpeed: 100, duration: 5, fade: 1, angle: -90 + 18 * i.double)
                bullet.color = .yellow
                bullet.colorBlendFactor = 1
                finalBlastWave.append(bullet)
            }
            
            let finalBlastWaveVolley = Volley.init(bullets: finalBlastWave, rounds: 1, timeBetweenRounds: 0, aim: .none)
            finalBlastWaveVolley.sound = "shot01.m4a"
            
            for i in 0...5 {
                let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "CircleBullet"), speed: 200, angle: (360/6) * i.double)
                bullet.color = .yellow
                bullet.colorBlendFactor = 1
                bullet.acceleration = -300
                bullet.duration = 2
                bullet.setScale(3)
                bullet.setNextStep(.single(finalBlastWaveVolley))
                finalBlast.append(bullet)
            }
            
            let finalBlastVolley = Volley.init(bullets: finalBlast, rounds: 1, timeBetweenRounds: 0, aim: .none)
            finalBlastVolley.sound = "shot01.m4a"
            finalWave.append([finalBlastVolley])
            
            finalShot.setNextStep(.sequenceOfGroups(finalWave))
            
            let finalVolley = Volley.init(bullets: [finalShot], rounds: 1, timeBetweenRounds: 0, aim: .none)
            finalVolley.sound = "shot01.m4a"
            
            let finalGun = Gun.init(delay: 2, reload: 1000, volleys: .single(finalVolley), position: .init(x: 0, y: -125))
            boss4.addGun(finalGun)
            
            wave.append(boss)
            waves.append(wave)
        }
        
        wave1()
        wave2()
        wave1()
        wave3()
        wave4()
        wave5()
        wave1()
        wave6()
        bossWave()
        
    }
    
}
