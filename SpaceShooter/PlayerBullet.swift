//
//  PlayerBullet.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 12/6/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import SpriteKit

class PlayerBullet: SKSpriteNode, Updateable{
    
    
    
    var target: Enemy?
    let power = 1
    let flightSpeed: CGFloat = 2000
    var active = false
    
    var angle: CGFloat{
        get {
            let difference = CGPoint(x: (target?.position.x)! - position.x, y: (target?.position.y)! - position.y)
            return atan2(difference.x, difference.y)
        }
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
    }
    
    convenience init(){
        self.init(image: #imageLiteral(resourceName: "Bullet"))
        name = "bullet"
        colorBlendFactor = 1.0
        color = UIColor.red
        zPosition = 4
        position = CGPoint(x:0, y:-340)
        
        physicsBody = SKPhysicsBody(texture: texture!, size: size)
        physicsBody!.affectedByGravity = false
        physicsBody!.linearDamping = 0
        physicsBody!.collisionBitMask = 0
        physicsBody!.categoryBitMask = 2
        

    }

    func homeIn(){
        let x = flightSpeed * sin(angle)
        let y = flightSpeed * cos(angle)
        zRotation = 0.0 - angle
        physicsBody?.velocity = CGVector(dx: x, dy: y)
    }
    
    func shoot(at node: SKNode, from point: CGPoint) {
        
        position = point
        target = node as? Enemy
        active = true
        homeIn()
    }
    


    override func copy() -> Any {
        let copy = super.copy() as! PlayerBullet
        return copy
    }
    
    func update(_ time: Double) {
        if target != nil {
            if target!.isDead == false{
                homeIn()
            }
            else {
                target = nil
            }
        }
        if parent != nil{
            if !intersects(parent!){
                removeFromParent()
            }
        }
    }
    
}
