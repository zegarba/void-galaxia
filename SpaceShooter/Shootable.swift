//
//  Shootable.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/4/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

protocol Shootable: class{
    
    var angle: Double! {get set}
    var position: CGPoint {get set}
    
    func copy() -> Shootable
    
    func shoot(scene: SKScene)
    func hitTarget()
    func update(_ time: Double)
    
}

extension Shootable where Self: SKSpriteNode {

    
    private func random(from start: Double, to finish: Double) -> Double{
        let difference = finish - start
        return start + drand48() * difference
    }
    
    func randomize(_ value: Double?, with dev: Double?) -> Double?{
        if value != nil{
            if dev != nil {
                return random(from: value! - dev!, to: value! + dev!)
            }
            else {
                return value!
            }
        }
        else{
            return nil
        }
    }
    
    func addToGame(scene: SKScene){
        
        name = "bullet"
        scene.addChild(self)
        let angle = self.angle.toRadians
        // set sprite rotation
        zRotation = CGFloat(-angle)
        zPosition = 5
    }
    
    func setCollisionDetection(){
        
        name = "bullet"
        physicsBody = SKPhysicsBody(texture: texture!, size: size)
        physicsBody!.affectedByGravity = false
        physicsBody!.linearDamping = 0
        physicsBody!.collisionBitMask = 0
        physicsBody!.categoryBitMask = 1
    }
}

