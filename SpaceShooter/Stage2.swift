//
//  Stage2.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 1/19/17.
//  Copyright © 2017 Zach Garbarino. All rights reserved.
//

import Foundation
import SpriteKit

extension GameScene {
    
    func stage2(){
        
        stageNo = 2
        
        // set background
        let stars = SKEmitterNode(fileNamed: "stage2Background.sks")!
        stars.zPosition = -1
        stars.name = "stars"
        addChild(stars)
        
        var wave1 = [Enemy]()
        
        let straightShot = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 125, angle: 0)
        let straightVolley = Volley(bullets: [straightShot], rounds: 2, timeBetweenRounds: 1, aim: .none, angle: 0, angleChange: nil, sound: "shot01.m4a")
        
        for i in 1...6 {
            let d = Double(i)
            for j in 0...1 {
                let e = Double(j)
                let gun = Gun(delay: 0.8, reload: 20, volleys: .single(straightVolley) ,position: .init(x: 0, y: 0))
                gun.delayRand = 1
                
                
                let enemy = Enemy(priority: 1, life: 2, delay: d * 0.2, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
                enemy.spin(-0.5)
                enemy.setMovement(start: [.init(x: -1250, y: 0, speed: 350, moveType: .linear, speedType: .constant, delay: 0)], loop: nil, remove: true)
                enemy.position = .init(x: 628, y: 220 - e*60)
                enemy.addGun(gun)
                
                enemy.explosion = "explosion"
                enemy.explodeSound = "explosion01.m4a"
                
                wave1.append(enemy)
            }
        }
        
        for i in 1...6 {
            let d = Double(i)
            
            let gun = Gun(delay: 0.8, reload: 20, volleys: .single(straightVolley) ,position: .init(x: 0, y: 0))
            gun.delayRand = 1
            
            let enemy = Enemy(priority: 1, life: 2, delay: d * 0.2, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
            enemy.spin(0.5)
            enemy.setMovement(start: [.init(x: 1250, y: 0, speed: 350, moveType: .linear, speedType: .constant, delay: 0)], loop: nil, remove: true)
            enemy.position = .init(x: -628, y: 190)
            enemy.addGun(gun)
            
            enemy.explosion = "explosion"
            enemy.explodeSound = "explosion01.m4a"
            
            wave1.append(enemy)
        }
     
        waves.append(wave1)
        
        var wave2 = [Enemy]()
        var wave3 = [Enemy]()
        var wave5 = [Enemy]()
        var wave6 = [Enemy]()
        for enemy in wave1{
            wave2.append(enemy.copy() as! Enemy)
            wave3.append(enemy.copy() as! Enemy)
            wave5.append(enemy.copy() as! Enemy)
            wave6.append(enemy.copy() as! Enemy)
        }
        waves.append(contentsOf: [wave2, wave3])
        
        var wave4 = [Enemy]()
        

        var greenBullets = [Shootable]()
        for i in 0...2 {
            let d = Double(i)
            
            let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 150, angle: -20 + d*20)
            bullet.color = .green
            bullet.colorBlendFactor = 0.8
            greenBullets.append(bullet)
        }
        let greenVolley = Volley(bullets: greenBullets, rounds: 1, timeBetweenRounds: 0, aim: .player, angle: 0, angleChange: nil, sound: "shot01.m4a")
        let gun2 = Gun(delay: 2.4, reload: 1.55, volleys: .single(greenVolley) ,position: .init(x: 0, y: -24))
        
        for i in 0...9{
            let c = CGFloat(i)
            let d = Double(i)
            let enemy = Enemy(priority: 1, life: 50, delay: d*1.5, sprite: #imageLiteral(resourceName: "BasicEnemy"))
            enemy.position = .init(x: -400 + (c * 200).truncatingRemainder(dividingBy: 1000) , y: 300)
            enemy.setMovement(start: [.init(x: 0, y: -100 + (c * -100).truncatingRemainder(dividingBy: 200), speed: 200, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
            enemy.colorBlendFactor = 0.25
            enemy.color = .blue
            enemy.addGun(gun2)
            enemy.explodeSound = "explosion03.m4a"
            enemy.explosion = "explosion01"
            wave4.append(enemy)
        }

        waves.append(wave4)
        waves.append(wave5)
        waves.append(wave6)
        
        var wave7 = [Enemy]()
        
        var tripleShots = [Shootable]()
        for i in 0...2 {
            let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 120, angle: 25 - i.double * 25)
            bullet.color = .magenta
            bullet.colorBlendFactor = 1
            tripleShots.append(bullet)
        }
        let tripleVolley = Volley(bullets: tripleShots, rounds: 3, timeBetweenRounds: 0.3, aim: .playerFreeze, angle: 10, angleChange: -10, sound: "laser01.m4a")
        
        let tripleShot = Gun(delay: 2, reload: 1.2, volleys: .single(tripleVolley), position: .init(x: 0, y: -24))
        
        
        for i in 0...2 {
            let enemy = Enemy(priority: 1, life: 120, delay: 0.2 * i.double, sprite: #imageLiteral(resourceName: "BasicEnemy"))
            enemy.color = .magenta
            enemy.colorBlendFactor = 0.5
            
            enemy.position = .init(x: -300 + i * 300, y: 320)
            enemy.setMovement(start: [.init(x: 0, y: -100, time: 1, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
            enemy.explosion = "explosion01"
            enemy.explodeSound = "explosion01.m4a"
            enemy.addGun(tripleShot)
            wave7.append(enemy)
        }
        
        waves.append(wave7)
        
        var wave8 = [Enemy]()
        
        var crossBullets = [Shootable]()
        for i in 0...3{
            let d = Double(i)
            let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 70, angle: d * 90)
            bullet.color = .red
            bullet.colorBlendFactor = 1
            crossBullets.append(bullet)
        }
        
        let spinVolley = Volley(bullets: crossBullets, rounds: 6, timeBetweenRounds: 0.5, aim: .none, angle: 0, angleChange: 15, sound: "shot01.m4a")
        
        let spinGun = Gun(delay: 3, reload: 0.5, volleys: .single(spinVolley), position: .init(x: 0, y: 0))
        
        for i in 0...1 {
            let d = Double(i)
            let enemy = Enemy(priority: 2, life: 150, delay: 1 + d * 0.1, sprite: #imageLiteral(resourceName: "ShurikenEnemy"))
            enemy.color = .gray
            enemy.colorBlendFactor = 0.7
            enemy.position = .init(x: -275 + d * 550, y: 320)
            enemy.setMovement(start: [.init(x: 0, y: -150, time: 1.5, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
            enemy.spin(-1/12)
            enemy.addGun(spinGun)
            enemy.explodeSound = "explosion01.m4a"
            enemy.explosion = "explosion01"
            wave8.append(enemy)
        }
        
        let bigEnemy = Enemy(priority: 1, life: 300, delay: 1.4, sprite: #imageLiteral(resourceName: "LargeEnemy"))

        
        let iLaser = InstantLaser.init(sprite: #imageLiteral(resourceName: "Laser"), angle: 0, delay: 1, duration: 2, width: 0.7)
        iLaser.color = .gray
        iLaser.colorBlendFactor = 0.7
        
        let iLaserVolley = Volley(bullets: [iLaser], rounds: 5, timeBetweenRounds: 0.2, aim: .playerFreeze, angle: -60, angleChange: 30, sound: "laser02.m4a")
        
        let laserGuns = Gun(delay: 5, reload: 2.5, volleys: .single(iLaserVolley), position: .init(x: 0, y: -64))
        
        bigEnemy.addGun(laserGuns)
        bigEnemy.position = .init(x: 0, y: 320)
        bigEnemy.setMovement(start: [.init(x: 0, y: -180, time: 2, moveType: .linear, speedType: .slowDown, delay: 0)], loop: nil, remove: false)
        bigEnemy.phaseOut = .fadeOut(withDuration: 1)
        bigEnemy.explodeSound = "longexplosion01.m4a"
        bigEnemy.explosion = "bossExplosion"
        bigEnemy.color = .orange
        bigEnemy.colorBlendFactor = 0.7
        wave8.append(bigEnemy)
        
        
        waves.append(wave8)
        
        var wave9 = [Enemy]()
        
        let boss = Enemy(priority: 1, life: 500, delay: 1, sprite: #imageLiteral(resourceName: "Boss01"))
        boss.phaseOut = SKAction.move(to: .init(x: 0, y: 140), duration: 3)
        boss.phaseOut.timingMode = .easeOut
        boss.position = .init(x: 0, y: 300)
        boss.phaseIn = SKAction.move(by: .init(dx: 0, dy: -160), duration: 2)
        boss.phaseIn?.timingMode = .easeOut
        let leftWing = EnemyPart(sprite: #imageLiteral(resourceName: "Boss01LeftWing"))
        leftWing.position = .init(x: -120, y: 45)
        leftWing.deathAnim = SKAction.group([.move(by: .init(dx: -200, dy: 70), duration: 3), .fadeOut(withDuration: 3)])
        leftWing.explosion = "bossExplosion.sks"
        boss.explodeSound = "longexplosion01.m4a"
        boss.addPart(leftWing)
        let rightWing = EnemyPart(sprite: #imageLiteral(resourceName: "Boss01RightWing"))
        rightWing.position = .init(x: 120, y: 45)
        boss.addPart(rightWing)
        boss.isBoss = true
        boss.bossMusic = "Boss01.m4a"
        
        boss.setMovement(start: nil, loop: [.init(x: 30, y: 0, time: 1, moveType: .linear, speedType: .slowDown, delay: 0), .init(x: -60, y: 0, time: 2, moveType: .linear, speedType: .speedUpSlowDown, delay: 0), .init(x: 30, y: 0, time: 1, moveType: .linear, speedType: .speedUp, delay: 0)], remove: false)
        
        let bossBullet0 = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 220, angle: 0)
        bossBullet0.color = .red
        bossBullet0.colorBlendFactor = 1
        bossBullet0.angleRand = 30
        let bossVolley0 = Volley.init(bullets: [bossBullet0], rounds: 10, timeBetweenRounds: 0.2, aim: .player)
        bossVolley0.sound = "shot01.m4a"
        let leftWingGun0 = Gun.init(delay: 1, reload: 0.2, volleys: .single(bossVolley0), position: .init(x: -110, y: -1))
        let rightWingGun0 = Gun(delay: 1, reload: 0.2, volleys: .single(bossVolley0), position: .init(x: 110, y: -1))
        
        let laserShot0 = Bullet.init(sprite: #imageLiteral(resourceName: "Laser"), speed: 160, angle: 0)
        laserShot0.yScale = 1.1
        let laserVolley0 = Volley(bullets: [laserShot0], rounds: 5, timeBetweenRounds: 0.1, aim: .player)
        laserVolley0.sound = "laser01.m4a"
        for i in 0...1 {
            let gun = Gun(delay: 2, reload: 3, volleys: .single(laserVolley0), position: .init(x: -55 + i.cgfloat * 110, y: -45))
            boss.addGun(gun)
        }
        
        boss.addGuns([leftWingGun0, rightWingGun0])
        
        let boss1 = Enemy(priority: 1, life: 500, delay: 0, sprite: #imageLiteral(resourceName: "Boss01"))
        boss1.position = .init(x: 0, y: 140)
        let rightWing1 = EnemyPart(sprite: #imageLiteral(resourceName: "Boss01RightWing"))
        rightWing1.position = .init(x: 120, y: 45)
        rightWing1.deathAnim = SKAction.group([.move(by: .init(dx: 200, dy: 70), duration: 3), .fadeOut(withDuration: 3)])
        rightWing1.explosion = "bossExplosion.sks"
        boss1.explodeSound = "longexplosion01.m4a"
        boss1.addPart(rightWing1)
        
        let bullet1 = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 250, angle: 0)
        bullet1.color = .red
        bullet1.colorBlendFactor = 1
        
        let volleySweepLeft1 = Volley(bullets: [bullet1], rounds: 100, timeBetweenRounds: 0.025, aim: .none)
        volleySweepLeft1.angle = 70
        volleySweepLeft1.angleChange = -1
        volleySweepLeft1.sound = "shot02.m4a"
        let volleySweepRight1 = Volley(bullets: [bullet1], rounds: 100, timeBetweenRounds: 0.025, aim: .none)
        volleySweepRight1.angle = -50
        volleySweepRight1.angleChange = 1
        volleySweepRight1.sound = "shot02.m4a"
        
        let leftSweep1 = Gun.init(delay: 1, reload: 4, volleys: .single(volleySweepLeft1), position: .init(x: 111, y: -1))
        let rightSweep1 = Gun.init(delay: 4, reload: 4, volleys: .single(volleySweepRight1), position: .init(x: 111, y: -1))
        
        let laser1 = Laser.init(sprite: #imageLiteral(resourceName: "Laser"), width: 1, travelSpeed: 1200, duration: 0.6, fade: 0.2, angle: 0)
        let laserVolley1l = Volley(bullets: [laser1], rounds: 6, timeBetweenRounds: 0.2, aim: .none)
        laserVolley1l.sound = "laser02.m4a"
        laserVolley1l.angle = -50
        laserVolley1l.angleChange = 25
        let laserGun1l = Gun.init(delay: 2, reload: 5.475, volleys: .single(laserVolley1l), position: .init(x: 0, y: -62))
        
        let laserVolley1r = Volley(bullets: [laser1], rounds: 6, timeBetweenRounds: 0.2, aim: .none)
        laserVolley1r.sound = "laser02.m4a"
        laserVolley1r.angle = 50
        laserVolley1r.angleChange = -25
        let laserGun1r = Gun.init(delay: 5.5, reload: 5.475, volleys: .single(laserVolley1r), position: .init(x: 0, y: -62))
        
        boss1.addGuns([leftSweep1, rightSweep1, laserGun1l, laserGun1r])
        
        boss1.isBoss = true
        boss1.phaseOut = SKAction.wait(forDuration: 3)
        
        boss.nextPhase = boss1
        
        let boss2 = Enemy.init(priority: 1, life: 1200, delay: 0, sprite: #imageLiteral(resourceName: "Boss01"))
        boss2.isBoss = true
        boss2.position = .init(x: 0, y: 140)
        
        
        var lasers2 = [Shootable]()
        for i in 0...40 {
            let bigLaser2 = InstantLaser.init(sprite: #imageLiteral(resourceName: "Laser"), angle: -40 + i.double * 2, delay: 2.5, duration: 7, width: 3)
            bigLaser2.color = .blue
            bigLaser2.colorBlendFactor = 0.5
            if i < 5 {
                bigLaser2.sound = "laser02.m4a"
            }
            lasers2.append(bigLaser2)
        }
        let bigLaserVolley2 = Volley(bullets: lasers2, rounds: 1, timeBetweenRounds: 0, aim: .player)
        bigLaserVolley2.sound = "laser01.m4a"
        let bigLaserGun2 = Gun(delay: 3, reload: 12, volleys: .single(bigLaserVolley2), position: .init(x: 0, y: -66))
        
        boss2.addGun(bigLaserGun2)
        
        var tripleShot2 = [Bullet]()
        for i in 0...2 {
            let bullet = Bullet.init(sprite: #imageLiteral(resourceName: "WhiteBullet"), speed: 250, angle: -12 + i.double * 12)
            bullet.color = .red
            bullet.colorBlendFactor = 0.8
            tripleShot2.append(bullet)
        }
        let tripleVolley2 = Volley(bullets: tripleShot2, rounds: 30, timeBetweenRounds: 0.2, aim: .player)
        tripleVolley2.sound = "shot01.m4a"
        for i in 0...1 {
            let gun = Gun(delay: 6, reload: 6.2, volleys: .single(tripleVolley2), position: .init(x: -55 + i.cgfloat * 110, y: -45))
            boss2.addGun(gun)
        }
        
        let shake = SKAction.repeat(.sequence([.move(by: .init(dx: 10, dy: 0), duration: 0.1), .move(by: .init(dx: -10, dy: 0), duration: 0.1)]), count: 40)
        boss2.color = .darkGray
        boss2.colorBlendFactor = 0
        let fade = SKAction.sequence([.colorize(withColorBlendFactor: 1, duration: 4), .fadeOut(withDuration: 4)])
        boss2.phaseOut = SKAction.group([shake, fade])
        boss2.explosion = "bossExplosion02.sks"
        boss2.explodeSound = "boss02explosion.m4a"
        boss1.nextPhase = boss2
        
        wave9.append(boss)
        waves.append(wave9)
    }
    
}
