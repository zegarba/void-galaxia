//
//  GameViewController.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 11/14/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        srand48(Int(arc4random()))

        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "TitleScreen") {
                let note = SKScene(fileNamed: "Notice")
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                note?.scaleMode = .aspectFill
                
                let transition = SKTransition.fade(withDuration: 4)
                
                // Present the scene
                view.presentScene(note)
                view.presentScene(scene, transition: transition)
            }
            
            view.ignoresSiblingOrder = true
            
            
            
            view.showsFPS = false
            view.showsNodeCount = false
            //view.showsPhysics = true
            }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
}
