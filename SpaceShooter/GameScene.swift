//
//  GameScene.swift
//  SpaceShooter
//
//  Created by Zach Garbarino on 11/14/16.
//  Copyright © 2016 Zach Garbarino. All rights reserved.
//

import SpriteKit
import GameController

class GameScene: SKScene, SKPhysicsContactDelegate {
    private var lastUpdateTime : TimeInterval = 0

    var waves = [[Enemy]]()
    var started = false
    var enemies = [Enemy]()
    static var angle:Double! = 0
    
    
    
    var stageNo = 1
    var over = false
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let data = aDecoder.decodeObject(forKey: "waves")
        if data != nil {
            waves = data as! [[Enemy]]
        }
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(waves, forKey: "waves")
    }
    
    override func sceneDidLoad() {
        self.lastUpdateTime = 0
    }
    
    override func didMove(to view: SKView) {
        
        MusicPlayer.fadeOut()
        
        // set the stage
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        
        setStage()
        
        let ready = childNode(withName: "ready")!
        ready.alpha = 0
        let readyFade = SKAction.sequence([.fadeIn(withDuration: 1), .fadeOut(withDuration: 1)])
        ready.run(.repeat(readyFade, count: 2)){
            self.craftPauseMenu()
            ready.removeFromParent()
            self.newWave()
            self.started = true
            self.childNode(withName: "barrierCount")?.isHidden = false
        }
        
        if let controller = Remote.sharedInstance.controller{
            if let motion = controller.motion {
                motion.valueChangedHandler = { motion in
                    GameScene.angle = controller.rotation(motion: motion)
                }
            }
        }
        
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(pause))
        gestureRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.menu.hashValue)]
        view.addGestureRecognizer(gestureRecognizer)
        gestureRecognizer.name = "pause"
        
        let selectRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectButton))
        selectRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.select.hashValue)]
        view.addGestureRecognizer(selectRecognizer)
        
        let rightRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(moveSelect))
        rightRecognizer.direction = UISwipeGestureRecognizerDirection.right
        view.addGestureRecognizer(rightRecognizer)
        rightRecognizer.isEnabled = false
        
        let leftRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(moveSelect))
        rightRecognizer.direction = UISwipeGestureRecognizerDirection.left
        view.addGestureRecognizer(leftRecognizer)
        leftRecognizer.isEnabled = false
        
    }
    
    override func willMove(from view: SKView) {
        MusicPlayer.play()
    }
    
    @objc func pause(){
        if view!.viewWithTag(3)?.isHidden == true {
            isPaused = true
            alpha = 0.5
            MusicPlayer.pause()
            let pause = view!.viewWithTag(3) as! SKView
            pause.isHidden = false
            pause.isPaused = false
            (pause.scene as! PauseMenu).openMenu()
            for gesture in view!.gestureRecognizers!{
                if gesture as? UISwipeGestureRecognizer != nil{
                    gesture.isEnabled = true
                }
                if gesture.name == "pause" {
                    gesture.isEnabled = false
                }
            }
        }
        // Necessary check if pause menu is actually there
        else if view!.viewWithTag(3) != nil{
            isPaused = false
            alpha = 1
            MusicPlayer.resume()
            let pause = view!.viewWithTag(3) as! SKView
            pause.isHidden = true
            pause.isPaused = true
            (pause.scene as! PauseMenu).closeMenu()
            for gesture in view!.gestureRecognizers!{
                if gesture as? UISwipeGestureRecognizer != nil{
                    gesture.isEnabled = false
                }
                if gesture.name == "pause" {
                    gesture.isEnabled = true
                }
            }
        }
    }
    
    @objc func selectButton(){
        let pauseView = view?.viewWithTag(3) as? SKView
        let pauseScene = pauseView?.scene as? PauseMenu
        if pauseView?.isHidden == false {
            pauseScene?.selectButton()
        }
    }
    
    @objc func moveSelect(){
        let pauseView = view!.viewWithTag(3) as! SKView
        let pauseScene = pauseView.scene as! PauseMenu
        if pauseView.isHidden == false {
            pauseScene.moveSelect()
        }
    }
    
    
    func newWave(){
        enemies = waves[0]
        for enemy in enemies {
            enemy.addToScene(self)
            if enemy.isBoss {
                setLifeBar(enemy.life)
            }
        }
    }
    
    func setLifeBar(_ life: Int){
        let label = childNode(withName: "bosslife")!
        label.alpha = 0
        label.run(.fadeIn(withDuration: 0.5))
        let lifeBar = childNode(withName: "lifebar")!
        lifeBar.yScale = 0
        lifeBar.alpha = 0
        lifeBar.userData!["life"] = life
        lifeBar.run(.group([.scaleY(to: 56, duration: 1), .fadeIn(withDuration: 1)]))
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        (childNode(withName: "crosshair") as? Crosshair)?.unlock()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if isPaused == false {
            for touch in touches {
                let location = touch.location(in: self)
                let previousLocation = touch.previousLocation(in: self)
                let translation = CGPoint(x: location.x - previousLocation.x, y: location.y - previousLocation.y)
                let move = CGPoint(x: 0.0 - translation.y * 1.5, y: translation.x)
                (childNode(withName: "crosshair") as? Crosshair)?.move(by: move)
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        (childNode(withName: "crosshair") as? Crosshair)?.lock()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        (childNode(withName: "crosshair") as? Crosshair)?.lock()
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        // Initialize _lastUpdateTime if it has not already been
        if (self.lastUpdateTime == 0) {
            self.lastUpdateTime = currentTime
        }
        
        // Calculate time since last update
        var dt = currentTime - self.lastUpdateTime
        
        // Workaround for pausing not pausing dt as well
        if dt > 0.3 {
            dt = 1.0 / 60.0
        }
        
        // let children update their positions according to how they work
        for child in children{
            (child as? Updateable)?.update(dt)
        }
        
        if !over {
            
            // Clear our dead enemies from the list, and start the next wave (or finish the level) if none are left
            enemies = enemies.filter({$0.isSet == false || $0.parent != nil})
            if enemies.isEmpty && started{
                if waves.count == 1{
                    stageClear()
                }
                else {
                    waves.removeFirst()
                    newWave()
                }
            }
        }
        
        self.lastUpdateTime = currentTime
    }
    func didBegin(_ contact: SKPhysicsContact) {
        if contact.bodyA.node?.name == "bullet" {
             bulletHit(contact.bodyA.node!, target: contact.bodyB.node!)
        }
        else if contact.bodyB.node?.name == "bullet" {
            bulletHit(contact.bodyB.node!, target: contact.bodyA.node!)
        }

    }
    func bulletHit(_ bullet: SKNode, target: SKNode){
        if target.name == "enemy"{
            if (bullet as! PlayerBullet).active == true {
                (target as! Enemy).takeDamage((bullet as! PlayerBullet).power)
                (bullet as! PlayerBullet).removeFromParent()
            }
        } else if target.name == "enemy part"{
            if (bullet as! PlayerBullet).active == true {
                (target.parent as! Enemy).takeDamage((bullet as! PlayerBullet).power)
                (bullet as! PlayerBullet).removeFromParent()
            }
        } else if target.name == "player"{
            (bullet as! Shootable).hitTarget()
            (target as! Player).takeDamage()
        }
    }

    
    func moveToTitle(){
        
        Remote.sharedInstance.controller?.microGamepad?.valueChangedHandler = nil
        
        view?.gestureRecognizers?.forEach(view!.removeGestureRecognizer)
        
        self.run(.wait(forDuration: 5)){
            MusicPlayer.selectTrack(0)
            let titleScene = SKScene(fileNamed: "TitleScreen.sks")!
            titleScene.scaleMode = .aspectFill
            self.view!.presentScene(titleScene, transition: SKTransition.doorsCloseHorizontal(withDuration: 1))
        }
    }
    
    func gameOver(){
        
        over = true
        moveToTitle()
        
        let gameOver = SKLabelNode(fontNamed: "VCROSDMono")
        gameOver.text = "Game Over"
        gameOver.alpha = 0
        gameOver.fontSize = 40
        gameOver.zPosition = 20
        gameOver.run(.group([.fadeIn(withDuration: 0.25), .scaleX(to: 3, duration: 3.5)]))
        gameOver.run(.sequence([.wait(forDuration: 3), .fadeOut(withDuration: 0.5)]))
        addChild(gameOver)
    }
    
    func stageClear(){
        
        over = true
        
        childNode(withName: "crosshair")?.run(.sequence([.fadeOut(withDuration: 0.5), .removeFromParent()]))
        if stageNo == 0 || stageNo == 3 {
            gameClear()
        }
        else {
            nextStage()
        }
    }
    
    func nextStage(){
        
        MusicPlayer.fanfare()
        
        let player = childNode(withName: "player")
        player?.physicsBody?.contactTestBitMask = 0
        player?.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        player?.run(.wait(forDuration: 1)){
            player?.physicsBody!.velocity = CGVector(dx: 0, dy: -20)
        }
        player?.run(.sequence([.wait(forDuration: 3), .group([.applyForce(CGVector.init(dx: 0, dy: 2), duration: 1), .playSoundFileNamed("zoomforward01.m4a", waitForCompletion: false)])]))
        

        
        let clear = SKLabelNode(fontNamed: "VCROSDMono")
        clear.text = "Stage Clear!"
        clear.alpha = 0
        clear.fontSize = 40
        clear.zPosition = 20
        clear.run(.repeat(.sequence([.fadeIn(withDuration: 0.5), .fadeOut(withDuration: 0.5)]), count: 3))
        
        run(.wait(forDuration: 5)){
            self.stageNo += 1
            MusicPlayer.selectTrack(self.stageNo)
            let nextStage = GameScene.getStage(self.stageNo)
            self.view?.presentScene(nextStage, transition: .fade(withDuration: 1))
        }
        
        addChild(clear)
        
        
    }
    
    func gameClear(){
        moveToTitle()
        
        
        MusicPlayer.fanfare()
        let clear = SKLabelNode(fontNamed: "VCROSDMono")
        clear.text = "GAME CLEAR"
        clear.alpha = 0
        clear.fontSize = 80
        clear.run(.fadeIn(withDuration: 3))
        clear.run(.scaleX(to: 1.2, duration: 3))
        addChild(clear)
        
        let player = childNode(withName: "player")
        player?.physicsBody?.contactTestBitMask = 0
        player?.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        player?.run(.wait(forDuration: 1)){
            player?.physicsBody!.velocity = CGVector(dx: 0, dy: -20)
        }
        player?.run(.sequence([.wait(forDuration: 3), .group([.applyForce(CGVector.init(dx: 0, dy: 2), duration: 1), .playSoundFileNamed("zoomforward01.m4a", waitForCompletion: false)])]))
        
        
    }
}
